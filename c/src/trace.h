#ifndef __TRACE_H__
#define __TRACE_H__

#ifdef TRACEON
#define TRACE(fmt, ...) fprintf(stderr, "T%ld - " fmt, syscall(SYS_gettid), \
                          ## __VA_ARGS__); 
#else
#define TRACE(fmt, ...)
#endif


#endif /* __TRACE_H__ */

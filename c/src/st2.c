#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>
#include "st2.h"
#include "trace.h"

/*
 * Sundell and Tsigas Doubly Linked List Implementation
 *
 * TODO REFERENCES !!!
 */

/* apply the mark to o */
#define MARK(o)       (((unsigned long)o)|1)

/* return 0/1 if o is unmarked / marked respectively */
#define ISMARKED(o)   (int)(((unsigned long)o)&1)

/* return the reference portion of o */
#define REF(o)        (void *)(((unsigned long)o)&~1)

/* perform a compare and swap on o; or = original ref nr = new ref */
#define CAS(o,or,nr) __sync_bool_compare_and_swap(o,or,nr)

/* dereference o yielding NULL if o is marked */
#define DREF(o)        (ISMARKED(o) ? NULL : REF(o))

static void   push_common (Node * node, Node * next);
static Node * help_insert (Node * prev, Node * node);
static void   mark_prev   (Node * node);
static void   help_delete (Node * node);
static void   print_node  (Node * n);
static Node * malloc_node ();
 
void print_node(Node * n)
{
  fprintf(stderr, "\t%p [P=%p, N=%p]\n", n, n->prev, n->next);
}

struct ST2 {
  Node * head, * tail;
};

Node * malloc_node()
{
  Node * n = calloc(1, sizeof(Node));
  assert(n != NULL);
}

void st2_print(ST2 * st2)
{
  Node * n;
  TRACE("LIST HEAD");
  for (n = st2->head; n != st2->tail; n = n->next)
    {
      print_node(n);
    }
  print_node(st2->tail);
  fputs("LIST TAIL\n", stderr);
}
void
st2_free(ST2 * st2)
{
  free(st2);
}


void
st2_push_left(ST2 * st2, void * val)
{
  Node * node, * prev, * next;
  TRACE("st2_push_left: st2=%p, val=%p\n", st2, val);
  node = malloc_node();
  node->obj = val;
  prev = st2->head;
  next = REF(prev->next);

  for (;;)
    {
      if (REF(prev->next) != next || ISMARKED(prev->next))
        {
          next = REF(prev->next);
          continue;
        }
      node->prev = prev;
      node->next = next;
      if (CAS(&prev->next, next, node))
          break;
    }
    push_common(node, next);
}

ST2 *
st2_init()
{
  ST2 * rv;
  rv = malloc(sizeof(ST2));
  assert(rv != NULL);

  rv->head = malloc(sizeof(Node));
  assert(rv->head != NULL);
  rv->tail = malloc(sizeof(Node));
  assert(rv->tail != NULL);

  rv->head->prev = NULL;
  rv->head->next = rv->tail;
  rv->tail->prev = rv->head;
  rv->tail->next = NULL;
  TRACE("st2_init: head=%p, tail=%p\n", rv->head, rv->tail);
  return rv;
}

void *
st2_pop_right(ST2 * st2)
{
  void * rv;
  Node * next, * node;
  rv = NULL;
  next = st2->tail;
  node = REF(next->prev);

  TRACE("st2_pop_right: node=%p, next=%p\n", node, next);
  
  for (;;)
    {
      Node * test = node->next;

      if (REF(test) != next || ISMARKED(test))
        {
          node = help_insert(node, next);
          continue;
        }
      if (node == st2->head)
          break;
      if (CAS(&node->next, next, MARK(next)))
        {
          Node * prev = REF(node->prev);
          help_delete(node);
          help_insert(prev, next);
          rv = node->obj;
          break;
        }
    }
  
  return rv;
}

void *
st2_pop_left(ST2 * st2)
{
  void * rv = NULL;
  Node * prev = st2->head;
  Node * node;

  TRACE("st2_pop_left: prev=%p, prev->next=%p, tail=%p\n", prev,
    prev->next, st2->tail);

  for (;;)
    {
      node = REF(prev->next);
      if (node == NULL || node == st2->tail) break;

      Node * link1 = node->next;

      if (ISMARKED(link1))
        {
          help_delete(node);
          continue;
        }
      if (CAS(&node->next, link1, MARK(link1)))
        {
          help_delete(node);
          Node * next = REF(node->next);
          help_insert(prev, next);
          rv = node->obj;
          //free(node);
          break;
        }
    }
  return rv;
}

void
help_delete(Node * node)
{
  Node * last, *prev, *next;
  mark_prev(node);
  last = NULL;
  prev = REF(node->prev);
  next = REF(node->next);

  TRACE("help_delete: node=%p, prev=%p, next=%p\n", node, prev, next);

  for (;;)
    {
      if (prev == next) break;
      if (ISMARKED(next->next))
        {
          mark_prev(next);
          next = REF(next->next);
          continue;
        }
      Node * prev2 = prev->next;

      if (ISMARKED(prev2))
        {
          if (last != NULL)
            {
              mark_prev(prev);
              Node * next2 = REF(prev->next);
              if (CAS(&last->next, REF(prev), next2))
                {
                  ;
                }
              else
                {
                  prev = last;
                  last = NULL;
                }
            }
          else
            {
              prev = REF(prev->prev);
            }
          continue;
        }
      if (prev2 != node)
        {
          last = prev;
          prev = prev2;
          continue;
        }
      if (CAS(&prev->next, REF(node), REF(next)))
          break;
    }
}
/*
 *
 */
void push_common(Node * node, Node * next)
{
  TRACE("push_common: node=%p, next=%p\n", node, next);
  for (;;)
    {
      Node * link = next->prev;
      if (ISMARKED(link)
            || REF(node->next) != next
            && ISMARKED(node->next))
          break;
      if (CAS(&next->prev, link, node))
        {
          if (ISMARKED(node->prev))
            {
              help_insert(node, next);
            }
          break;
        }
    }
}

void
mark_prev(Node * node)
{
  TRACE("mark_prev: node=%p\n", node);
  for (;;)
    {
      Node * prev = node->prev;
      if (ISMARKED(prev) || CAS(&prev, prev, MARK(prev)))
        {
          break;
        }
    }
}
Node *
help_insert(Node * prev, Node * node)
{
  assert(prev != NULL);
  Node * last = NULL;
  TRACE("help_insert: prev=%p, node=%p\n", prev, node);

  for (;;)
    {
      Node * prev2 = prev->next;
      if (ISMARKED(prev2))
        {
          if (last != NULL)
            {
              mark_prev(prev);
              Node * next2 = REF(prev->next);
              CAS(&last->next, prev, next2);
              prev = last;
              last = NULL;
            }
          else
            {
              prev = REF(prev->prev);
            }
          continue;
        }
      Node * link1 = node->prev;
      if (ISMARKED(link1))
          break;
      if (prev2 != node)
        {
          last = prev;
          prev = prev2;
          continue;
        }
      if (link1 == prev)
          break;
      if (REF(prev->next) == node
          && CAS(&node->prev, link1, REF(prev)))
        {
          if (!ISMARKED(prev->prev))
              continue;
          break;
        }
    }
  return prev;
}

int CASNode(Node ** node, Node * old_ref, Node * new_ref, BOOL mark)
{
  Node * ref = mark ? (Node *)MARK(new_ref) : new_ref;
  return CAS(node, old_ref, ref);
}

int CASI(int ** obj, int * old_ref, int * new_ref, BOOL mark)
{
  int * ref = mark ? (int *)MARK(new_ref) : new_ref;
  return __sync_bool_compare_and_swap (obj, old_ref, ref);
}

/*
int main (int c, char ** v)
{
  int x = 1;
  int y = 2;
  int * px = &x; 
  int * py = &y;
  int b;

  for (;;)
    {
      b = CASI(&px, px, py, TRUE);
      if (b > 0)
          break;
    }
  printf("x=%d marked=%d\n", *((int*)REF(px)), ISMARKED(px));
}

*/

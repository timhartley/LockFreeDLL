#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "st2.h"

#define E 10
static int array[E];

void seed_array() {
  for (int i = 0; i < E; i++)
      array[i] = i;
}

int main (int c, char ** v)
{
  int checksum, checksum2;
  ST2 * st2 = st2_init();
  checksum = checksum2 = 0;

  seed_array();
  for (int i = 0; i < E; i++)
    {
      st2_push_left(st2, &array[i]);
      checksum += array[i];
    }
  st2_print(st2);
  for (;;)
    {
      void * v = st2_pop_right(st2);
      if (v == NULL) break;
      st2_print(st2);
      fprintf(stderr, "GOT %d\n", *(int *)v);
      checksum2 += *(int *)v;
    }
  assert(checksum == checksum2);
}


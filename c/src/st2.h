#ifndef _ST2_H_
#define _ST2_H_

/*
 * Public interface of the Sundell and Tsigas Doubly Linked List
 * algorithm without the cursor operations.
 */

/* forward declaration of opaque ST2 queue object */
typedef struct ST2 ST2;

/*
 * Create an ST2 doubly linked list
 */
extern ST2  * st2_init        ();

/*
 * Push an item onto the head of the queue.
 */
extern void   st2_push_left   (ST2 * st2, void * val);

/*
 * Push an item onto the tail of the queue.
 */
extern void   st2_push_right  (ST2 * st2, void * val);

/*
 * Pop an item from the head of the queue. 
 */
extern void * st2_pop_left    (ST2 * st2);

/*
 * Pop an item from the tail of the queue.
 */
extern void * st2_pop_right   (ST2 * st2);

/*
 * Print a representation of the queue to stderr.
 */
extern void   st2_print       (ST2 * st2);

/*
 * Returns the backoff value specified at compile time.
 */
extern int    st2_backoff_val ();


#endif /* _ST2_H_ */

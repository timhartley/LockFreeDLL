#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>
#include "st2.h"
#include "trace.h"

/*
 * Sundell and Tsigas Doubly Linked List Implementation
 *
 * TODO REFERENCES !!!
 */
typedef struct Node {
  void * obj;
  struct Node * next;
  struct Node * prev;
} Node;

struct ST2 {
  Node * head, * tail;
};

/* apply the mark to o */
#define MARK(o)       (((unsigned long)o)|1)

/* return 0/1 if o is unmarked / marked respectively */
#define ISMARKED(o)   (int)(((unsigned long)o)&1)

/* return the reference portion of o */
#define REF(o)        (void *)(((unsigned long)o)&~1)

/* perform a compare and swap on o; or = original ref nr = new ref */
#define CAS(o,or,nr) __sync_bool_compare_and_swap(o,or,nr)

/* dereference o yielding NULL if o is marked */
#define DREF(o)        (ISMARKED(o) ? NULL : REF(o))

#ifndef BACKOFF
# define BACKOFF 0 
#endif

static void   push_common (Node * node, Node * next);
static Node * help_insert (Node * prev, Node * node);
static void   mark_prev   (Node * node);
static void   help_delete (Node * node);
static void   print_node  (Node * n);
static Node * malloc_node ();
static void   backoff();

void print_node(Node * n)
{
  fprintf(stderr, "\t%p [P=%p, N=%p]\n", n, n->prev, n->next);
}


Node * malloc_node()
{
  Node * n = calloc(1, sizeof(Node));
  assert(n != NULL);
}

int
st2_backoff_val()
{
  return BACKOFF;
}

void
backoff()
{
  int i;

  for (i = 0; i < BACKOFF; i++)
      ;
}
void st2_print(ST2 * st2)
{
  Node * n;
  TRACE("LIST HEAD");
  for (n = st2->head; n != st2->tail; n = n->next)
    {
      print_node(n);
    }
  print_node(st2->tail);
  fputs("LIST TAIL\n", stderr);
}
void
st2_free(ST2 * st2)
{
  free(st2);
}

void set_mark(Node ** node)
{
  Node * n;
  for (;;)
    {
      n = *node;
      if (ISMARKED(n) || CAS(node, n, MARK(n)))
          break;
    }
}

Node *
correct_prev(Node * prev, Node * node)
{
  TRACE("correct_prev: prev=%p, node=%p\n", prev, node);
  Node * last = NULL;
  for (;;)
    {
      TRACE("correct_prev: ITER prev=%p (p.n=%p), node=%p (n.p=%p)\n",
        prev, prev->next, node, node->prev);
      Node * link1 = node->prev;

      if (ISMARKED(link1))
        {
          break;
        }
      Node * prev2 = prev->next;
      if (ISMARKED(prev2))
        {
          if (last != NULL)
            {
              set_mark(&prev->prev);
              CAS(&last->next, prev, REF(prev2));
              prev = last;
              last = NULL;
              TRACE("continue 1\n");
              continue;
            }
          prev = REF(prev->prev);
          TRACE("continue 2\n");
          continue;
        }
      if (prev2 != node)
        {
          last = prev;
          prev = prev2;
          TRACE("continue 3 prev2=%p, last=%p, prev=%p\n",
            prev2, last, prev);
          continue;
        }
      if (CAS(&node->prev, link1, REF(prev)))
        {
          TRACE("correct_prev: CAS OK np=%p, link1=%p, prev=%p",
            node->prev, link1, prev);
          if (ISMARKED(prev->prev))
            {
              TRACE("continue 4\n");
              continue;
            }
          break;
        }
      else
        {
          TRACE("correct_prev: CAS FAIL np=%p, link1=%p, prev=%p",
            node->prev, link1, prev);
        }
      TRACE("Bottom\n");
      backoff();
    }
  return prev;
  //return NULL;
}

void
push_end(Node * node, Node * next)
{
  TRACE("push_end: node=%p, next=%p\n", node, next);
  for (;;)
    {
      Node * link1 = next->prev;

      if (ISMARKED(link) || node->next != REF(next))
        {
          break; 
        }
      if (CAS(&next->prev, link1, node))
        {
          if (ISMARKED(node->prev))
            {
              node = correct_prev(node, next);
            }
          break;
        }
      backoff();
    }
}
void
st2_push_left(ST2 * st2, void * val)
{
  Node * node, * prev, * next;
  TRACE("st2_push_left: st2=%p, val=%p\n", st2, val);
  node = malloc_node();
  node->obj = val;
  prev = st2->head;
  next = REF(prev->next);

  for (;;)
    {
      node->prev = prev;
      node->next = next;

      if (CAS(&prev->next, next, node))
        {
          TRACE("st2_push_left: cas prev->next=%p\n", prev->next);
          break;
        }
      next = REF(prev->next);
      backoff();
    }
  push_end(node, next);
}

ST2 *
st2_init()
{
  ST2 * rv;
  rv = malloc(sizeof(ST2));
  assert(rv != NULL);

  rv->head = malloc(sizeof(Node));
  assert(rv->head != NULL);
  rv->tail = malloc(sizeof(Node));
  assert(rv->tail != NULL);

  rv->head->prev = NULL;
  rv->head->next = rv->tail;
  rv->tail->prev = rv->head;
  rv->tail->next = NULL;
  TRACE("st2_init: head=%p, tail=%p\n", rv->head, rv->tail);
  return rv;
}

void *
st2_pop_right(ST2 * st2)
{
  void * rv;
  Node * next, * node;
  rv = NULL;
  next = st2->tail;
  node = REF(next->prev);

  TRACE("st2_pop_right: node=%p, next=%p\n", node, next);
  
  for (;;)
    {
      Node * test = node->next;

      if (REF(test) != next || ISMARKED(test))
        {
          node = correct_prev(node, next);
          continue;
        }
      if (node == st2->head)
          break;
      if (CAS(&node->next, next, MARK(next)))
        {
          Node * prev = REF(node->prev);
          prev = correct_prev(prev, next);
          rv = node->obj;
          /*
           * It is not possible to safely free nodes without implementing
           * the full memory management scheme since logically deleted
           * nodes may still be reached via inconsistent references.
           * For the purpose of the current experiment freeing memory
           * is omitted.
           */
          /* free(node); */
          break;
        }
      backoff();
    }
  
  return rv;
}

void
st2_push_right(ST2 * st2, void * val)
{
  Node * next, * prev, * node;
  node = malloc_node ();
  node->obj = val;
  next = st2->tail;
  prev = next->prev;

  for (;;)
    {
      node->prev = REF(prev);
      node->next = REF(next);

      if (CAS(&prev->next, REF(next), REF(node)))
        break;

      prev = correct_prev(prev, next);
      backoff();
    }
  push_end(node, next);
}

void *
st2_pop_left(ST2 * st2)
{
  void * rv = NULL;
  Node * prev = st2->head;
  Node * node;

  TRACE("st2_pop_left: prev=%p, prev->next=%p, tail=%p\n", prev,
    prev->next, st2->tail);

  for (;;)
    {
      TRACE("st2_pop_left: ITER\n");
      node = REF(prev->next);
      if (node == NULL || node == st2->tail) break;

      Node * next = node->next;

      if (ISMARKED(next))
        {
          set_mark(&node->prev);
          CAS(&prev->next, node, REF(next));
          continue;
        }
      if (CAS(&node->next, next, MARK(next)))
        {
          prev = correct_prev(prev, next);
          rv = node->obj;
          /*
           * It is not possible to safely free nodes without implementing
           * the full memory management scheme since logically deleted
           * nodes may still be reached via inconsistent references.
           * For the purpose of the current experiment freeing memory
           * is omitted.
           */
          // free(node);
          break;
        }
      backoff();
    }
  return rv;
}

void
help_delete(Node * node)
{
  Node * last, *prev, *next;
  mark_prev(node);
  last = NULL;
  prev = REF(node->prev);
  next = REF(node->next);

  TRACE("help_delete: node=%p, prev=%p, next=%p\n", node, prev, next);

  for (;;)
    {
      if (prev == next) break;
      if (ISMARKED(next->next))
        {
          mark_prev(next);
          next = REF(next->next);
          continue;
        }
      Node * prev2 = prev->next;

      if (ISMARKED(prev2))
        {
          if (last != NULL)
            {
              mark_prev(prev);
              Node * next2 = REF(prev->next);
              if (CAS(&last->next, REF(prev), next2))
                {
                  ;
                }
              else
                {
                  prev = last;
                  last = NULL;
                }
            }
          else
            {
              prev = REF(prev->prev);
            }
          continue;
        }
      if (prev2 != node)
        {
          last = prev;
          prev = prev2;
          continue;
        }
      if (CAS(&prev->next, REF(node), REF(next)))
          break;
    }
}
/*
 *
 */
void push_common(Node * node, Node * next)
{
  TRACE("push_common: node=%p, next=%p\n", node, next);
  for (;;)
    {
      Node * link = next->prev;
      if (ISMARKED(link)
            || REF(node->next) != next
            && ISMARKED(node->next))
          break;
      if (CAS(&next->prev, link, node))
        {
          if (ISMARKED(node->prev))
            {
              help_insert(node, next);
            }
          break;
        }
    }
}

void
mark_prev(Node * node)
{
  TRACE("mark_prev: node=%p\n", node);
  for (;;)
    {
      Node * prev = node->prev;
      if (ISMARKED(prev) || CAS(&prev, prev, MARK(prev)))
        {
          break;
        }
    }
}
Node *
help_insert(Node * prev, Node * node)
{
  assert(prev != NULL);
  Node * last = NULL;
  TRACE("help_insert: prev=%p, node=%p\n", prev, node);

  for (;;)
    {
      Node * prev2 = prev->next;
      if (ISMARKED(prev2))
        {
          if (last != NULL)
            {
              mark_prev(prev);
              Node * next2 = REF(prev->next);
              CAS(&last->next, prev, next2);
              prev = last;
              last = NULL;
            }
          else
            {
              prev = REF(prev->prev);
            }
          continue;
        }
      Node * link1 = node->prev;
      if (ISMARKED(link1))
          break;
      if (prev2 != node)
        {
          last = prev;
          prev = prev2;
          continue;
        }
      if (link1 == prev)
          break;
      if (REF(prev->next) == node
          && CAS(&node->prev, link1, REF(prev)))
        {
          break;
          //if (!ISMARKED(prev->prev))
          //    break;
        }
    }
  return prev;
}

/*
int CASNode(Node ** node, Node * old_ref, Node * new_ref, BOOL mark)
{
  Node * ref = mark ? (Node *)MARK(new_ref) : new_ref;
  return CAS(node, old_ref, ref);
}

int CASI(int ** obj, int * old_ref, int * new_ref, BOOL mark)
{
  int * ref = mark ? (int *)MARK(new_ref) : new_ref;
  return __sync_bool_compare_and_swap (obj, old_ref, ref);
}

int main (int c, char ** v)
{
  int x = 1;
  int y = 2;
  int * px = &x; 
  int * py = &y;
  int b;

  for (;;)
    {
      b = CASI(&px, px, py, TRUE);
      if (b > 0)
          break;
    }
  printf("x=%d marked=%d\n", *((int*)REF(px)), ISMARKED(px));
}

*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include "st2.h"
#define NE 1000000

typedef struct TData {
  ST2 * st2;
  unsigned int checksum;
} TData;

static int array[NE] = {0};

static pthread_t THREAD_IDS[128] = {0};
static TData TDATA[128] = {0};
static unsigned int checksum = 0;
static struct timespec start = {0};
static struct timespec end = {0};
static int THREADS;

void seed_array()
{
  int i;
  for (i = 0; i < NE; i++)
    {
      array[i] = i;
      checksum += i;
    }
}

void fill_list(ST2 * st2)
{
  int i;
  for (i = 0; i < NE; i++)
      st2_push_left(st2, &array[i]);
}

void *
thread_start(void * arg)
{
  unsigned int checksum = 0;
  TData  * t = (TData *)arg;
  for (;;)
    {
      void * v = st2_pop_right(t->st2);
      if (v == NULL) break;
      // printf("GOT: %d\n", *(int *)v);
      checksum += *(int*)v;
    }
  t->checksum = checksum;
  return NULL;
}

void start_threads(ST2 * st2)
{
  int lv;
  int i;
  for (i = 0; i < THREADS; i++)
    {
      TDATA[i].st2 = st2;
      lv = pthread_create(&THREAD_IDS[i], NULL, thread_start, &TDATA[i]);
      assert(lv == 0);
    }
}

void
join_threads()
{
  int i, lv;
  unsigned int checksum2 = 0;
  for (i = 0; i < THREADS; i++)
    {
      lv = pthread_join(THREAD_IDS[i], NULL);
      if (lv != 0)
        {
          fprintf(stderr, "ERROR pthread_join %d\n", lv); 
          exit(1);
        }
      checksum2 += TDATA[i].checksum;
    }
  // fprintf(stderr, "checksum=%u, checksum2=%u\n", checksum, checksum2);
  assert(checksum == checksum2);
}


void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result)
{
  if ((stop->tv_nsec - start->tv_nsec) < 0)
    {
      result->tv_sec = stop->tv_sec - start->tv_sec - 1;
      result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
    }
    else
    {
      result->tv_sec = stop->tv_sec - start->tv_sec;
      result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }
}

int main (int c, char ** v)
{
  struct timespec t;
  ST2 * st2;
  if (c != 2)
    {
      fprintf(stderr, "Usage: %s <nthreads>\n", *v);
      exit(0);
    }
  THREADS = atoi(v[1]);
  fprintf(stderr, "%s threads=%d, elements=%d, backoff=%d\n",
    *v, THREADS, NE, st2_backoff_val());
  st2 = st2_init();
  seed_array();
  fill_list(st2);
  clock_gettime(CLOCK_REALTIME, &start);
  start_threads(st2);
  join_threads();
  clock_gettime(CLOCK_REALTIME, &end);
  timespec_diff(&start, &end, &t);

  // fprintf(stderr, "tv_sec=%ld, tv_nsec=%ld\n", t.tv_sec, t.tv_nsec);
//  printf("ST2: %s elements=%d, threads=%d\n", v[0], NE, THREADS);
  printf("%ld\n", t.tv_sec * 1000000000 + t.tv_nsec);
	exit(0);	
}


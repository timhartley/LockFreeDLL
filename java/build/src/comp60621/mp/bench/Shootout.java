package comp60621.mp.bench;

public abstract class Shootout {

	private static int ELEMENTS = 100000;
	
	
	public static void main(String [] args) {
		int [] threads;
		if (args != null && args.length > 0) {
			ELEMENTS = Integer.parseInt(args[0]);
			threads = new int [args.length - 1];
			
			for (int i = 1; i < args.length; i++) {
				threads[i - 1] = Integer.parseInt(args[i]);
			}
		}
		else {
			threads = new int[] {1,2,4};
		}
		
		for (int t : threads) {
			System.out.printf("Threads: %d, elements per thread=%d\n", t, ELEMENTS / t);

//			run(t, ELEMENTS/t);
			System.out.println("===================");
		}

	}
	


}

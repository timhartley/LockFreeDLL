package comp60621.mp.bench;

/**
 * 
 * 
 *
 */
public interface BenchMark {

	/**
	 * 
	 * @return
	 */
	public void run ();
	
	//public String name();
	
	public long elapsedTime();
}

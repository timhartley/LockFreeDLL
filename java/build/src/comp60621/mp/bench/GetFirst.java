package comp60621.mp.bench;

import java.util.Deque;

public class GetFirst implements BenchMark {

	private int nthreads;
	
	private int elements;
	private Deque<Integer> deque;
	
	private long elapsedTime;
//	private Deque<Integer> scratch;
	
	public GetFirst(int nthreads, int elements, Deque<Integer> deque/*, Deque <Integer> scratch*/) {
		this.nthreads = nthreads;
		this.elements = elements;
		this.deque = deque;
//		this.scratch = scratch;
	}
	
	public long elapsedTime() {
		return elapsedTime;
	}
	
	@Override
	public void run() {
		Runner [] threads = new Runner [nthreads];
		int checksum = 0, checksum2 = 0;
		for (int i = 0; i < elements; i++) {
			deque.add(i);
			checksum += i;
//			scratch.add(i);
		}
		
		for (int i = 0; i < nthreads; i++) {
			//threads[i] = new Thread(() -> getFirst(deque));
			threads[i] = new Runner(deque);
		}
//		Thread warmup = new Thread(() -> getFirst(scratch));
//		
//
//		warmup.start();
//		
//		try {
//			warmup.join();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		System.gc();
		
		long start = System.nanoTime();
		for (int i = 0; i < nthreads; i++) {
			threads[i].start();
		}
		
		for (int i = 0; i < nthreads; i++) {
			try {
				threads[i].join();
				checksum2 += threads[i].checksum();
//				System.out.print("T" + Thread.currentThread().getId() + "=" + threads[i].count() + " ");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		elapsedTime = System.nanoTime() - start;
//		System.out.println("");
	}
	
	class Runner extends Thread {
		
		
		private int checksum = 0;
		
		private int count;
		
		private final Deque<Integer> deque;
		
		Runner(Deque<Integer> dq) {
			deque = dq;
		}
		
		int count() {
			return count;
		}
		
		int checksum() {
			return checksum;
		}

	
		@Override
		public void run() {
			for (;;) {
				Integer i = deque.pollFirst();
				if (i == null)
					break;
				checksum += i;
				count++;
//				System.out.println("T" + Thread.currentThread().getId() + "=" + count() + " ");
			}			
		}
	}

}

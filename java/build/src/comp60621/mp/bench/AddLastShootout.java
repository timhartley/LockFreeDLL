package comp60621.mp.bench;

import java.util.concurrent.ConcurrentLinkedDeque;

import comp60621.mp.ndll.NDLinkedList;
import comp60621.mp.stll.STLinkedList2;
import comp60621.mp.stll.STLinkedList3;

public class AddLastShootout {
	private static int RUNS = 10;
	private static int ELEMENTS = 500000;
	
	
	public static void main(String [] args) {
		int [] threads;
		if (args != null && args.length > 0) {
			ELEMENTS = Integer.parseInt(args[0]);
			threads = new int [args.length - 1];
			
			for (int i = 1; i < args.length; i++) {
				threads[i - 1] = Integer.parseInt(args[i]);
			}
		}
		else {
			threads = new int[] {1,2,4};
		}
		
		for (int t : threads) {
			System.out.printf("Threads: %d, elements per thread=%d\n", t, ELEMENTS / t);
			runND(t, ELEMENTS / t);
			runCLD(t, ELEMENTS / t);
			runST2(t, ELEMENTS / t);
			runST3(t, ELEMENTS / t);
			System.out.println("===================");
		}

	}
	
	
	
	static void runND (int threads, int elements) {
		System.out.println("Niloufar Shafiei Linked List");
		AddLast [] res = new AddLast [2];
		for (int i = 0; i < RUNS; i++) {
			res[0] = new AddLast(threads, elements, new NDLinkedList<Integer>());
			res[0].run();
			res[1] = new AddLast(threads, elements, new NDLinkedList<Integer>());
			res[1].run();
			System.out.println(res[1].elapsedTime());
			System.gc();
		}

	}
	
	static void runCLD (int threads, int elements) {
		System.out.println("JDK Concurrent Linked Deque");
		AddLast [] res = new AddLast [2];
		for (int i = 0; i < RUNS; i++) {
			res[0] = new AddLast(threads, elements, new ConcurrentLinkedDeque<Integer>());
			res[1] = new AddLast(threads, elements, new ConcurrentLinkedDeque<Integer>());
			res[0].run();
			res[1].run();
			System.out.println(res[1].elapsedTime());
			System.gc();
		}
		System.out.println("-------------------");
	}
	
	static void runST2 (int threads, int elements) {
		System.out.println("Sundel Tsigas II");
		AddLast [] res = new AddLast [2];
		for (int i = 0; i < RUNS; i++) {
			res[0] = new AddLast(threads, elements, new STLinkedList2<Integer>());
			res[1] = new AddLast(threads, elements, new STLinkedList2<Integer>());
			res[0].run();
			res[1].run();
			System.out.println(res[1].elapsedTime());
			System.gc();
		}
		System.out.println("-------------------");
	}
	
	static void runST3 (int threads, int elements) {
		System.out.println("Sundel Tsigas III");
		AddLast [] res = new AddLast [2];
		for (int i = 0; i < RUNS; i++) {
			res[0] = new AddLast(threads, elements, new STLinkedList3<Integer>());
			res[1] = new AddLast(threads, elements, new STLinkedList3<Integer>());
			res[0].run();
			res[1].run();
			System.out.println(res[1].elapsedTime());
			System.gc();
		}
		System.out.println("-------------------");
	}
}

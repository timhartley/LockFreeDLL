package comp60621.mp.bench;

import java.util.concurrent.ConcurrentLinkedDeque;

import comp60621.mp.ndll.NDLinkedList;
import comp60621.mp.stll.STLinkedList2;
import comp60621.mp.stll.STLinkedList3;

public class GetLastShootout {
	private static int RUNS = 10;
	private static int ELEMENTS = 100000;
	
	
	public static void main(String [] args) {
		System.out.println("PollLast");
		int [] threads;
		if (args != null && args.length > 0) {
			ELEMENTS = Integer.parseInt(args[0]);
			threads = new int [args.length - 1];
			
			for (int i = 1; i < args.length; i++) {
				threads[i - 1] = Integer.parseInt(args[i]);
			}
		}
		else {
			threads = new int[] {1,2,4};
//			threads = new int[] {4};
		}
		
		for (int t : threads) {
			System.out.printf("Threads: %d, elements=%d\n", t, ELEMENTS);
			runND(t, ELEMENTS);
			runCLD(t, ELEMENTS);
			runST2(t, ELEMENTS);
			runST3(t, ELEMENTS);
			System.out.println("===================");
		}

	}
	
	
	
	static void runND (int threads, int elements) {
		System.out.println("Niloufar Shafiei Linked List");
		BenchMark [] res = new BenchMark [2];
		for (int i = 0; i < RUNS; i++) {
			res[0] = new GetLast(threads, elements, new NDLinkedList<Integer>());
			res[0].run();
			res[1] = new GetLast(threads, elements, new NDLinkedList<Integer>());
			res[1].run();
			System.out.println(res[1].elapsedTime());
			System.gc();
		}

	}
	
	static void runCLD (int threads, int elements) {
		System.out.println("JDK Concurrent Linked Deque");
		BenchMark [] res = new BenchMark [2];
		for (int i = 0; i < RUNS; i++) {
			res[0] = new GetLast(threads, elements, new ConcurrentLinkedDeque<Integer>());
			res[1] = new GetLast(threads, elements, new ConcurrentLinkedDeque<Integer>());
			res[0].run();
			res[1].run();
			System.out.println(res[1].elapsedTime());
			System.gc();
		}
		System.out.println("-------------------");
	}
	
	static void runST2 (int threads, int elements) {
		System.out.println("Sundel Tsigas II");
		BenchMark [] res = new BenchMark [2];
		for (int i = 0; i < RUNS; i++) {
			res[0] = new GetLast(threads, elements, new STLinkedList2<Integer>());
			res[1] = new GetLast(threads, elements, new STLinkedList2<Integer>());
			res[0].run();
			res[1].run();
			System.out.println(res[1].elapsedTime());
			System.gc();
		}
		System.out.println("-------------------");
	}
	
	static void runST3 (int threads, int elements) {
		System.out.println("Sundel Tsigas III");
		BenchMark [] res = new BenchMark [2];
		for (int i = 0; i < RUNS; i++) {
			res[0] = new GetLast(threads, elements, new STLinkedList3<Integer>());
			res[1] = new GetLast(threads, elements, new STLinkedList3<Integer>());
			res[0].run();
			res[1].run();
			System.out.println(res[1].elapsedTime());
			System.gc();
		}
		System.out.println("-------------------");
	}
}

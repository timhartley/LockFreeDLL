package comp60621.mp.stll;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicMarkableReference;

/**
 * Sundell and Tsigas 2005 lock free doubly linked list.
 * 
 *
 * @param <E>
 */
public class STLinkedList2<E> implements Deque<E> {

	private final boolean TRACE;
	private final Node HEAD;
	private final Node TAIL;
	
//	AtomicInteger traceHelpInsert = new AtomicInteger(0);
//	AtomicInteger traceCASFail = new AtomicInteger(0);
	public STLinkedList2() {
		this(false);
	}
	@SuppressWarnings("unchecked")
	public STLinkedList2 (boolean trace) {
		HEAD = new Node((E)"HEAD", null, null);
		TAIL = new Node((E)"TAIL", HEAD, null);
		HEAD.setNext(TAIL, false);
		TRACE = trace;
	}
	
	E popLeft() {
		E rv = null;
		Node prev = HEAD;
		Node node;
		for (;;) {
			node = prev.next();
			if (node == null || node == TAIL) { break;}
			
			boolean [] del = {false};
			
			Node link1 = node.next(del);
			
			if (del[0]) {
				helpDelete(node);
				continue;
			}

			if (node.casNext(link1, del[0], link1, true)) {
				helpDelete(node);
				Node next = node.next();
				helpInsert(prev, next);
				rv = node.value;
				break;
			}
		}
		if (node != null) {removeCrossReference(node);}
//		trace("popped: " + toString());
		return rv;
	}
	
	E popRight() {
		E rv = null;
		Node next = TAIL;
		Node node = next.prev();
		boolean [] del = {false};		
		for (;;) {
			Node test = node.next(del);
			if (test != next || del[0]) {
				node = helpInsert(node, next);
				continue;
			}
			if (node == HEAD) {
				break; // i.e. return null
			}
			if (node.casNext(next, false, next, true)) {
				helpDelete(node);
				Node prev = node.prev();
				helpInsert(prev, next);
				rv = node.value(); // ?
				break;
			}
//			else {
//				traceCASFail.incrementAndGet();
//				trace("popRight cas failed: expected=", node.next(), " del=", node.nextDelete(), ", new=", next);
//			}
		}
		
		return rv;
	}
	void pushLeft(E value) {
		Node node = new Node(value);
		Node prev = HEAD;
		Node next = prev.next();
//		boolean [] del = {false};
		for (;;) {
//			Node test = prev.next(del);
			if (prev.next() != next || prev.nextDelete()) {
				next = prev.next();
				continue;
			}
			node.setPrev(prev, false);
			node.setNext(next, false);
			if (prev.casNext(next, false, node, false)) {
				break;
			}
			else {
//				trace("Cas failed: current=" + prev.next() + ", next=" + next + ", node=" + node);
			}
		}
		pushCommon(node, next);
//		trace("List: " + toString());
	}
	
	void pushRight (E value) {
		Node node = new Node(value);
		Node next = TAIL;
		Node prev = next.prev();
		boolean [] del = {false};
		for (;;) {
			Node test = prev.next(del);
			if (test != next || del[0]) {
//			if (prev.next() != next || prev.nextDelete()) {
				prev = helpInsert(prev, next);
				continue;
			}
			node.setPrev(prev, false);
			node.setNext(next, false);
			if (prev.casNext(next, false, node, false)) {
				break;
			}
			else {
//				traceCASFail.incrementAndGet();
			}
		}
		pushCommon(node, next);
//		trace("pushedRight: ", toString());
	}
	
	void helpDelete(Node node) {
//		trace("helpDelete: node=" + node);
		markPrev(node);
		Node last = null;
		Node prev = node.prev();
		Node next = node.next();
		
		for (;;) {
			if (prev == next) {break;}
			if (next.nextDelete()) {
//				trace("HERE: next=" + next);
				markPrev(next);
				next = next.next();
				continue;
			}
			Node prev2 = prev.dnext();
			if (prev2 == null) {
				if (last != null) {
					markPrev(prev);
					Node next2 = prev.next();
					if (last.casNext(prev, false, next2, false)) {
						;
					}
					else {
						prev = last;
						last = null;
					}
				}
				else {
					prev = prev.prev();
				}
				continue;
			}
			if (prev2 != node) {
				last = prev;
				prev = prev2;
				continue;
			}
			if (prev.casNext(node, false, next, false)) {
				break;
			}
		}
	}
	
	void removeCrossReference(Node node) {
//		trace("removeCrossReference: node=" + node);
//		for (;;) {
//			Node prev = node.prev().prev();
//			if (prev.prevDelete()) {
//				Node prev2 = prev.prev();
//			}
//		}
	}
	
	void pushCommon(Node node, Node next) {
//		trace ("pushCommon: node=" + node + ", next=" + next);
		for (;;) {
			boolean [] del = {false};
			Node link1 = next.prev(del);
			if (del[0] || node.next() != next && node.nextDelete() != false) {
				break;
			}
			if (next.casPrev(link1, del[0], node, false)) {
				if (node.prevDelete()) {
					helpInsert(node, next);
				}
				break;
			}
			// back off;
		}
	}
	
	void trace(String message, Object ... args) {
		if (TRACE) {
			System.out.print(message);
			if (args != null && args.length != 0) {
				for (Object o : args) {
					System.out.print(o == null ? "null" : o);
				}
			}
			System.out.print('\n');
		}
	}
	
	Node helpInsert(Node prev, Node node) {
//		trace("helpInsert: prev=" + prev, ", node=" + node);
//		traceHelpInsert.incrementAndGet();
		Node last = null;
		
		for (;;) {
			boolean [] del = {false};
			Node prev2 = prev.next(del);
			if (del[0]) {
				if (last != null) {
					markPrev(prev);
					Node next2 = prev.next();
					if (last.casNext(prev, false, next2, false)) {
						;
					}
					prev = last;
					last = null;
				}
				else {
					prev = prev.prev();
				}
				continue;
			}
			//del = {false};
			Node link1 = node.prev(del);
			if (del[0]) {
				break;
			}
			if (prev2 != node) {
				last = prev;
				prev = prev2;
				continue;
			}
			if (link1 == prev) {
				break;
			}
			if (prev.next() == node && node.casPrev(link1, del[0], prev, false)) {
				if (!prev.prevDelete()) {
					break;
				}
			}
		}
		return prev;
	}
	
	void markPrev(Node node) {
//		trace("markPrev: " + node);
		for (;;) {
			Node prev = node.prev();
			if (node.prevDelete() || node.casPrev(prev, false, prev, true)) {
				break;
			}
		}
	}
	
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (Node n = HEAD; n != TAIL; n = n.next()) {
			buffer.append(n + "; ");
		}
		buffer.append(TAIL);
		return buffer.toString();
		
	}
	
	void print() {
		System.out.println(toString());
	}
	
	private class Node {
		private E value;
		
		private AtomicMarkableReference<Node> nextRef;
		
		private AtomicMarkableReference<Node> prevRef;
		
		private Node(E value) {
			this(value, null, false, null, false);
		}
		
		private Node(E value, Node prev, Node next) {
			this(value, prev, false, next, false);
		}
		
		private Node(E value, Node prev, boolean prevDel, Node next, boolean nextDel) {
			this.value = value;
			nextRef = new AtomicMarkableReference<>(next, nextDel);
			prevRef = new AtomicMarkableReference<>(prev, prevDel);
		}
		
		private E value() {
			return value;
		}
		
		private Node dnext() {
			return nextDelete() ? null : next();
		}
		
		private Node dprev() {
			return prevDelete() ? null : prev();
		}
		
		private Node next() {
			return nextRef.getReference();
		}
		
		private Node prev() {
			return prevRef.getReference();
		}
		
		private Node next(boolean [] del) {
			return nextRef.get(del);
		}
		
		private Node prev(boolean [] del) {
			return prevRef.get(del);
		}
		
		private void setNext(Node next, boolean delete) {
			//nextRef = new AtomicMarkableReference<>(next, delete);
			nextRef.set(next, delete);
		}
		
		private void setPrev(Node prev, boolean delete) {
			//prevRef = new AtomicMarkableReference<>(next, delete);
			prevRef.set(prev, delete);
		}
		
		private boolean nextDelete() {
			return nextRef.isMarked();
		}
		
		private boolean prevDelete() {
			return prevRef.isMarked();
		}
		
		private boolean casNext(Node expected, boolean expectedDelete, Node node, boolean delete) {
			return nextRef.compareAndSet(expected, node, expectedDelete, delete);
		}
		
		private boolean casPrev(Node expected, boolean expectedDelete, Node node, boolean delete) {
			return prevRef.compareAndSet(expected, node, expectedDelete, delete);
		}
		
		public String toString() {
			return value
				+ " [P=" + (prev() == null ? "null" : prev().value) + (prevDelete() ? "(d)" : "")
				+ " N=" + (next() == null ? "null" : next().value) + (nextDelete() ? "(d)]" : "]");
		}
	}
	@Override
	public boolean addAll(Collection<? extends E> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		return HEAD.next() == TAIL;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(E e) {
		addLast(e);
		return true;
	}

	@Override
	public void addFirst(E e) {
		pushLeft(e);
		
	}

	@Override
	public void addLast(E e) {
		pushRight(e);
		
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<E> descendingIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E element() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E getFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E getLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean offer(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean offerFirst(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean offerLast(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E peek() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E peekFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E peekLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E poll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E pollFirst() {
		return popLeft();
	}

	@Override
	public E pollLast() {
		return popRight();
	}

	@Override
	public E pop() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void push(E e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public E remove() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E removeFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeFirstOccurrence(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E removeLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeLastOccurrence(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		int c = 0;
		for (Node n = HEAD; n != TAIL; n = n.next()) {
			c++;
		}
		return c;
	}

}

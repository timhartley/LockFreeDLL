package comp60621.mp.stll;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicMarkableReference;


/**
 * Sundell and Tsigas linked list
 * @author tim
 *
 * @param <E>
 */
public class STLinkedList<E> implements Deque <E>{

	private final Node HEAD;
	private final Node TAIL;
	
	static boolean TRACE = true;
	public STLinkedList (){
		HEAD = new Node((E)"HEAD", null, null, false);
		TAIL = new Node((E)"TAIL", HEAD, null, false);
		HEAD.next(TAIL);
		
	}
	
	void print (String message, PrintStream out) {
		out.print(message);
		print(out);
	}
	
	void trace(String message, Object ... args) {
		if (TRACE) {
			System.out.printf("%2d", Thread.currentThread().getId());
			System.out.print(" ");
			System.out.print(message);
			if (args != null && args.length != 0) {
				for (Object o : args) {
					System.out.print(o == null ? "null" : o);
				}
			}
			System.out.print('\n');
		}
	}
	
	private void deleteNext(Node node) {
//		trace("deleteNext: node=" + node);
		boolean delete = true;
		Node prev = node.prevDel();
		Node next = node.nextDel();
//		print("deleteNext: ", System.out);
		for (;;) {
			if (prev == next) {break;}
			if (next.delete()) {
				next = next.nextDel();
				continue;
			}
			Node prev2 = prev.next();
			//Link link1 = prev.link();
			if (prev2 == null) {
				if (!delete) {
					deleteNext(prev);
					delete = true;
				}
				prev = prev.prevDel();
				continue;
			}
			Link link1 = prev.link();
			if (prev2 != node) {
				trace("prev2 != node: prev2=", prev2, ", node=", node, ", prev=", prev, ", next=", next);
				delete = false;
				prev = prev2;

				continue;
			}
//			System.out.println("Link1: " + link1);
//			System.out.println("Current: " + prev.link());
//			System.out.println("NewNext: " + node.nextDel());
			if (prev.cas(link1, new Link(link1.prev, node.nextDel()))) {
				break;
			}
		}
	}
	
	private Node helpInsert(Node prev, Node node) {
		boolean delete = true;
		for(;;) {
			Node prev2 = prev.next();
			if (prev2 == null) {
				if (delete == false) {
					deleteNext(prev);
					delete = true;
				}
				prev = prev.prevDel();
				continue;
			}
			Link link1 = node.link();
			
			if (node.delete()) {
				break;
			}
			if (prev2 != node) {
				delete = false;
				prev = prev2;
				continue;
			}
			if (node.cas(link1,  new Link(prev, link1.next))) {
				if (prev.delete()) {continue;}
				break;
			}
		}
		return prev;
	}
	
	private void pushCommon(Node node, Node next) {
		Link link1 = next.link();
		
		for (;;) {
//			print(System.out);
			if (next.delete() || node.delete() || node.next() != next) {
				break;
			}
			if (next.cas(link1, new Link(node, link1.next))) {
				if (node.delete()) {
					helpInsert(node, next);
				}
				break;
			}
//			System.out.println("ZZ");
		}
	}
	
	/*
	 * Tail is getting its delete bit set somewhere which is causing problems 
	 *
	 */
	void removeCrossReference(Node node) {
//		System.out.println("Node: " + node + " link: " + node.link().toString());
//		print(System.out);
		for (;;) {
			Link link1 = node.link();
			Node prev = link1.prev;
			if (prev.delete()) {
				Node prev2 = prev.prevDel();
				node.link(prev2, link1.next, true);
				continue;
			}
			Node next = link1.next;
			
			if (next.delete()) {
				Node next2 = next.nextDel();
				node.link(link1.prev, next2, true);
				continue;
			}
			break;
		}
	}
	
	E popLeft() {
		E rv = null;
		Node prev = HEAD;
		Node node;
		
		for (;;) {
			node = prev.next();
			if (node == TAIL) { return null;}
			Link link1 = node.link();
			if (node.delete()) {
				deleteNext(node);
				continue;
			}
			Node next = link1.next;
			// check the current mark;
//			System.out.println("P: " + link1.prev + ", N: " + link1.next);
//			print ("popLeft: ", System.out);
			//if (node.cas(link1, new Link(link1.prev, link1.next), false, true)) {
			if (node.cas(link1, link1, false, true)) {
//				print ("afterCAS: ", System.out);
//				System.out.println("P: " + node.prev() + ", N: " + node.next());
//				System.out.println("AA");
				deleteNext(node);
//				System.out.println("BB");
//				print("After deleteNext: ", System.out);
				prev = helpInsert(prev, next);
//				System.out.println("CC");
				rv = node.value;
				break;
			}
		}
		removeCrossReference(node);
		return rv;
	}
	
	protected void pushLeft(E value) {
		Node n = new Node(value);
		Node prev = HEAD;
		Node next = HEAD.next();
		
		for (;;) {
			Link link1 = prev.link();
			if (link1.next != next) {
				next = prev.next();
				continue;
			}
			n.link(prev, link1.next, false);
			if (prev.cas(link1, new Link(link1.prev, n)))
				break;
		}
		pushCommon(n, next);
	}
	
	private class Link {
		private Node prev;
		
		private Node next;
		
		private Link (Node prev, Node next) {
			this.prev = prev;
			this.next = next;
		}
		
		public String toString() {
//			return new String("[P->" + prev == null ? "null" : prev.value + ", N->" + next == null ? "null" : next.value + "]");
			StringBuffer buffer = new StringBuffer("[P->");
			buffer.append(prev == null ? "null" : prev.value);
			buffer.append(", N->");
			buffer.append(next == null ? "null" : next.value);
			buffer.append("]");
			return buffer.toString();
		}
	}
	
	void print(PrintStream out) {
		Node n;
		for (n = HEAD; n != TAIL; n = n.nextDel()) {
//			System.out.println("[Link:" + n.link() +"]");
//			System.out.println("P=" + n.prevDel() + ", N=" + n.nextDel());
			//out.printf("%s>%c", n.toString(), n.next().prev() == n ? '<' : '*');
//			out.print(n.toString());
//			out.print(n.nextDel() != null ? '>' : '*');
			out.print(n);
			out.print(n.nextDel() != null ? '>' : '*');
		}
		out.printf("%s\n", n.toString());
	}
	 
	private class Node {
		private E value;
		
		private volatile AtomicMarkableReference<Link> linkRef;
		
		private Node(E value) {
			this.value = value;
		}
		private Node (E value, Node prev, Node next, boolean mark) {
			linkRef = new AtomicMarkableReference<Link>(new Link(prev, next), mark);
			this.value = value;
		}
		
		private Node next() {
			return linkRef.isMarked() ? null : nextDel();
		}
		
		private Node nextDel() {
			return linkRef.getReference().next;
		}
		
		private Node prev() {
			return linkRef.isMarked() ? null : prevDel();
		}
		
		private Node prevDel() {
			return linkRef.getReference().prev;
		}
		
		private void next(Node next) {
			linkRef.getReference().next = next;
		}
		
		private Link link() {
			return linkRef.getReference();
		}
		
		private void link(Node prev, Node next, boolean mark) {
			link(new Link(prev, next), mark);
		}

		private void link(Link link, boolean mark) {
			assert(this.linkRef == null);
			this.linkRef = new AtomicMarkableReference<Link>(link, mark);
		}
		
		private boolean cas(Link current, Link newLink, boolean currentMark, boolean newMark) {
//			System.out.println("currentLink: " + current + ", newLink: " + newLink);
			boolean b =  linkRef.compareAndSet(current, newLink, currentMark, newMark);
//			System.out.println("Link after CAS: " + link());
			return b;
		}
		
		private boolean cas(Link current, Link newLink) {
			return cas(current, newLink, delete(), false);
		}
		
		boolean delete() {
			return linkRef.isMarked();
		}
		
		
		public String toString() {
			return value + (delete() ? "(d)" : "") + " " + link();
		}
	}
	
	@Override
	public boolean addAll(Collection<? extends E> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		return HEAD.nextDel() == TAIL;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addFirst(E e) {
		pushLeft(e);
		
	}

	@Override
	public void addLast(E e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<E> descendingIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E element() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E getFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E getLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean offer(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean offerFirst(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean offerLast(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E peek() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E peekFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E peekLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E poll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E pollFirst() {
		return popLeft();
	}

	@Override
	public E pollLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E pop() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void push(E e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public E remove() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E removeFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeFirstOccurrence(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E removeLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeLastOccurrence(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

}

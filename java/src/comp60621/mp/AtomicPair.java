package comp60621.mp;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import sun.misc.Unsafe;

/**
 * 
 * Atomic object which can emulate a double word compare and swap (DCAS)
 * operation.
 *
 */
public class AtomicPair <T> {
	
	private Pair <T> pair;
	
	static Unsafe unsafe;
	
	private static long PAIR_OFFSET;
	static {

		try {
			Constructor <Unsafe> c = Unsafe.class.getDeclaredConstructor();
			c.setAccessible(true);
			unsafe = c.newInstance();
			c = Unsafe.class.getDeclaredConstructor();
			
			PAIR_OFFSET = unsafe.objectFieldOffset(AtomicPair.class.getDeclaredField("pair"));
		} catch (NoSuchMethodException | SecurityException | InstantiationException
				| IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchFieldException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public boolean compareAndSwap(T expected1, T new1, T expected2, T new2) {
		Pair <T> current = pair;
		
		return current.object1 == expected1 && current.object2 == expected2
				|| compareAndSwap(current, new Pair<T>(new1, new2));
		
		
	}
	
	private boolean compareAndSwap(Pair<T> currentPair, Pair<T> newPair) {
		return unsafe.compareAndSwapObject(this, PAIR_OFFSET, currentPair, newPair);
	}
	
	private class Pair <T> {
		
		private final T object1, object2;
		Pair (T object1, T object2) {
			this.object1 = object1;
			this.object2 = object2;
		}
	}

	public boolean cas(T t1, T t2) {
		return true;
	}
}

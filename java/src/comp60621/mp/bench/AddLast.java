package comp60621.mp.bench;

import java.util.Deque;

public class AddLast implements BenchMark {
	
	private final int nthreads;
	
	private final Deque<Integer> deque;
	
//	private final Deque<Integer> scratch;
	
	long elapsedTime;
	private final int elements;
	
	public AddLast(int nthreads, int elements, Deque <Integer> deque/*, Deque <Integer> scratch*/) {
		this.nthreads = nthreads;
		this.elements = elements;
		this.deque = deque;
//		this.scratch = scratch;
	}
	
	public long elapsedTime() {
		return elapsedTime;
	}
	
	public void run () {
		Thread [] threads = new Thread[nthreads];
		
		for (int n = 0; n < nthreads; n++) {
//			threads[n] = new Thread(() -> addLast(deque));
			threads[n] = new Thread(new Runnable() {

				@Override
				public void run() {
					addLast(deque);
					
				}
				
			});
		}

//		// warm up the JIT compilers
//		Thread warmup = new Thread(() -> addLast(scratch));
//		warmup.run();
//		try {
//			warmup.join();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		long start = System.nanoTime();
		
		for (Thread t : threads) {
			t.start();
		}
		
		for (Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		elapsedTime = System.nanoTime() - start;
		
	}
	private void addLast (Deque <Integer> dq) {
		for (int i = 0; i < elements; i++) {
			dq.addLast(i);
		}
	}

}

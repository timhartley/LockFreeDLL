package comp60621.mp.bench;

import java.util.Deque;

public class AddFirst implements BenchMark {
	
	private String name;
	
	private long elapsedTime;
	
	private final int nthreads;
	
	private final Deque<Integer> deque;
	
//	private final Deque<Integer> scratch;
	private final int elements;
	
	public AddFirst(int nthreads, int elements, Deque <Integer> deque/*, Deque <Integer> scratch*/) {
		this.nthreads = nthreads;
		this.elements = elements;
		this.deque = deque;
//		this.scratch = scratch;
	}
	
//	public AddFirst(String name, int elements, Deque <Integer> deque) {
//		this.nthreads = nthreads;
//		this.elements = elements;
//		this.deque = deque;
//		this.name = name;
//	}
	
	public String name() {
		return name;
	}
	
	public long elapsedTime() {
		return elapsedTime;
	}
	
	public void run () {
		int checksum = 0, checksum2 = 0;
		Runner [] threads = new Runner[nthreads];
		
		for (int n = 0; n < nthreads; n++) {
			//threads[n] = new Thread(() -> addFirst(deque));
			threads[n] = new Runner(deque, elements);
		}

		// warm up the JIT compilers
//		Thread warmup = new Thread(() -> addFirst(scratch));
//		warmup.run();
//		try {
//			warmup.join();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		long start = System.nanoTime();
		
		for (Thread t : threads) {
			t.start();
		}
		
		for (Runner t : threads) {
			try {
				t.join();
				checksum += t.checksum();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		elapsedTime = System.nanoTime() - start;
		
		for (;;) {
			Integer i = deque.pollFirst();
			if (i == null) break;
			checksum2 += i;
		}
		
		if (checksum != checksum2)
			throw new RuntimeException("Checksum failed: " + checksum + " != " + checksum2);
		
	}
	
	private class Runner extends Thread {
		
		private Deque <Integer> deque;
		
		private int checksum = 0;
		
		private int nelements;
		
		public Runner(Deque<Integer> dq, int n) {
			deque = dq;
			nelements = n;
		}
		int checksum() {
			return checksum;
		}
		
		public void run() {
			for (int i = 0; i < nelements; i++) {
				deque.addFirst(i);
				checksum += i;
			}
		}
	}
}

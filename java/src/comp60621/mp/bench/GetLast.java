package comp60621.mp.bench;

import java.util.Deque;

public class GetLast implements BenchMark {

	private int nthreads;
	
	private int elements;
	private Deque<Integer> deque;
	
//	private Deque<Integer> scratch;
	
	long elapsedTime;
	
	public GetLast(int nthreads, int elements, Deque<Integer> deque/*, Deque <Integer> scratch*/) {
		this.nthreads = nthreads;
		this.elements = elements;
		this.deque = deque;
//		this.scratch = scratch;
	}
	
	
	public long elapsedTime() {
		return elapsedTime;
	}
	
	@Override
	public void run() {
		int checksum = 0, checksum2 = 0;
		Runner [] runner = new Runner [nthreads];
		for (int i = 0; i < elements; i++) {
			deque.add(i);
			checksum += i;
//			scratch.add(i);
		}
		
		for (int i = 0; i < nthreads; i++) {
			runner[i] = new Runner(deque);
		}
		
		elapsedTime = System.nanoTime();
		for (int i = 0; i < nthreads; i++) {
			runner[i].start();
		}
			
		for (int i = 0; i < nthreads; i++) {
			try {
				runner[i].join();
				checksum2 += runner[i].checksum();
//				System.out.print("T" + runner[i].getId() + "=" + runner[i].count() + " ");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		elapsedTime = System.nanoTime() - elapsedTime;
//		System.out.println("");
		if (checksum != checksum2) {
			throw new RuntimeException("Checksum failed, expected=" + checksum + " got=" + checksum2);
		}
	}
	
	class Runner extends Thread {
		
		private int checksum = 0;
		
		private int count;
		
		private final Deque<Integer> deque;
		
		Runner(Deque<Integer> dq) {
			deque = dq;
		}
		
		int count() {
			return count;
		}
		
		int checksum() {
			return checksum;
		}
	
		@Override
		public void run() {
			for (;;) {
				Integer i = deque.pollLast();
				if (i == null)
					break;
				checksum += i;
				count++;
//				System.out.println("T" + Thread.currentThread().getId() + "=" + count() + " ");
			}			
		}
	}
}

package comp60621.mp.ndll;

import static comp60621.mp.ndll.NDLinkedList.State.ABORTED;
import static comp60621.mp.ndll.NDLinkedList.State.COMITTED;
import static comp60621.mp.ndll.NDLinkedList.State.COPIED;
import static comp60621.mp.ndll.NDLinkedList.State.IN_PROGRESS;
import static comp60621.mp.ndll.NDLinkedList.State.MARKED;
import static comp60621.mp.ndll.NDLinkedList.State.ORDINARY;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.NoSuchElementException;

import sun.misc.Unsafe;
import static comp60621.mp.ndll.NDLinkedList.BOOL.*;
/**
 * Niloufar Shafiei's Doubly linked list algorithm.
 * 
 * @author tim
 *
 * @param <E>
 */
public class NDLinkedList <E> implements Deque<E> {

	private final Node<E> HEAD;
	
	private final Node<E> TAIL;
	
	private final Node<E> EOL;
	
	private final Info <E> DUMMY;
	
	private final static boolean TRACE = false;
	
	String EOL_VALUE = new String("EOL");
	
	@SuppressWarnings("unchecked")
	private final E INVALID = (E)new Object();
	
    private final ThreadLocal<Cursor<E>> cursor =
            new ThreadLocal<Cursor<E>>() {
                @Override protected Cursor<E> initialValue() {
                    return new Cursor<E>(HEAD.next, false, false);
            }
        };
        
    private final ThreadLocal<PrintStream> printStream = 
    		new ThreadLocal<PrintStream>() {
    	protected PrintStream initialValue() {
    		try {
				return new PrintStream(new File("/tmp", Thread.currentThread().getName()));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				throw new RuntimeException(e);
			}
    	}
    };
        
	@SuppressWarnings("unchecked")
	public NDLinkedList () {
		DUMMY = new Info<E>(null, null, null, null, false, ABORTED);
		HEAD = new Node<E>((E)"HEAD", null, null, null, DUMMY, ORDINARY);
		EOL = new Node<E>((E)EOL_VALUE, null, HEAD, null, DUMMY, ORDINARY);
		TAIL = new Node<E>((E)"TAIL", null, EOL, null, DUMMY, ORDINARY);
		HEAD.next = EOL;
		EOL.next = TAIL;
//		trace("HEAD=", HEAD);
//		trace("EOL=", EOL);
//		trace("TAIL=", TAIL);
	}
	
	enum BOOL {
		TRUE, FALSE, INVALIDCURSOR;
	}
	void trace(String message, Object ... args) {
		if (TRACE) {
			printStream.get().print(Thread.currentThread().getName() + " " + message);
			if (args != null && args.length != 0) {
				for (Object o : args) {
					printStream.get().print(o == null ? "null" : o);
				}
			}
			printStream.get().print('\n');
			printStream.get().flush();;
		}
	}
	
	protected Cursor<E> resetCursor() {
		return setCursor(HEAD.next);
	}
	
	private Cursor<E> setCursor(Node<E> n) {
//		trace("setCursor: n=", n);
		Cursor<E> c = cursor.get();
//		trace(" Got cursor: " + c);
		//Cursor<E> c = new Cursor<E>(HEAD.next, false, false);
		c.node = n;
		c.invDel = c.invIns = false;
		return c;
	}
	
	private void updateCursor (Cursor<E> c) {
//		trace("updateCursor: c=", c);
		c.invIns = false;
		c.invDel = false;
		while (c.node.state != ORDINARY && c.prev().next != c.node) {
//			trace("updateCursor: c=", c, ", c.prev().next=", c.prev().next);
			if (c.node.state == COPIED) {
				c.invIns = true;
				c.node = c.node.copy;
			}
			//if (c.node.state == MARKED) {
			else {
				c.invDel = true;
				c.node = c.node.next;
			}
		}
//		trace("updateCursor: node=", c.node, ", invDel=", c.invDel, ", invIns=", c.invIns);
	}
	
	public E popLeft() {
		Cursor <E> c = resetCursor();
		return delete1(c);
	}
	
	protected boolean moveRight(Cursor<E> c) {
		updateCursor(c);
		// TODO need to handle invalid cursor
		if (c.invDel) return false;
		if (c.node.value == EOL_VALUE) return false;
		c.node = c.next();
		return true;
	}
	
	public E delete (Cursor<E> c) {
		
		for (;;) {
			updateCursor(c);
			
			if (c.invDel) {
				// TODO invalid cursor
//				trace("got invalid cursor: " + c);
				return null;
			}
			Node [] nodes = {c.prev(), c.node, c.next()};
			Info [] infos = {c.prev().info, c.node.info, c.next().info};
			
			if (checkInfo(nodes, infos)) {
				// move this outside the loop ?
				if (c.node.value == EOL_VALUE) {
					return null;
				}
				Info info = new Info(nodes, infos, c.next(), c.prev(), true, IN_PROGRESS);
				
				if (help(info)) {
					E e = c.node.value;
					c.node = c.next();
					return e;
				}
			}
		}
	}
	
	public E delete1 (Cursor<E> c) {
//		trace("delete1: c=", c);
		E rv = null;
		for (;;) {
			updateCursor(c);
			
			if (c.invDel) {
				// TODO invalid cursor
				//trace("got invalid cursor: " + c);
				//return INVALID;
				continue;
			}
//			if (c.node.value == EOL_VALUE) {
//				return null;
//			}
			Node <E> np = c.prev(); // previous node
			Node <E> nc = c.node;   // current node
			Node <E> nn = c.next(); // next node
			
			Info <E> ip = np.info; // previous info
			Info <E> ic = nc.info; // current info
			Info <E> in = nn.info; // next info
			
			if (checkInfo1(np, nc, nn, ip, ic, in)) {
				if (nc.value == EOL_VALUE) {
//					trace("delete1: @EOL np=", np, ", nc=", nc, ", nn=", nn);
					break;
				}
				@SuppressWarnings("unchecked")
				Info <E> info = new Info<E>(new Node[] {np, nc, nn}, new Info []{ip, ic, in}, nn, np, true, IN_PROGRESS);
				
				if (help(info)) {
					rv = nc.value;
//					c.node = nn;
					break;
				}
			}
		}
//		trace("delete1: deleted=", rv);
		return rv;
	}
	
	public BOOL delete2 (Cursor<E> c) {
//		trace("delete2: c=", c);
		for (;;) {
			updateCursor(c);
			
			if (c.invDel) {
				// TODO invalid cursor
				//trace("got invalid cursor: " + c);
				//return INVALIDCURSOR;
				continue;
			}
			if (c.node.value == EOL_VALUE) {
				return FALSE;
			}
			Node <E> np = c.prev(); // previous node
			Node <E> nc = c.node;   // current node
			Node <E> nn = c.next(); // next node
			
			Info <E> ip = np.info; // previous info
			Info <E> ic = nc.info; // current info
			Info <E> in = nn.info; // next info
			
			if (checkInfo1(np, nc, nn, ip, ic, in)) {
				if (nc.value == EOL_VALUE) {
					return FALSE;
				}
				@SuppressWarnings("unchecked")
				Info <E> info = new Info<E>(new Node[] {np, nc, nn}, new Info []{ip, ic, in}, nn, np, true, IN_PROGRESS);
				
				if (help(info)) {
					c.node = nn;
					return TRUE;
				}
			}
		}
	}
	
	protected E first() {
		Cursor<E> c = resetCursor();
		return get(c);
	}
	
	protected E last() {
		Cursor<E> c = setCursor(EOL);
		if (moveLeft(c) != TRUE) return null;
		return get(c);
	}
	
	protected E get() {
		return get(cursor.get());
	}
	
	protected E get(Cursor<E> c) {
//		trace("get: c=", c);
		updateCursor(c);
		if (c.invDel) return null;
		return c.node.value;
	}

	
	protected boolean insertBefore(Cursor <E> c, E value) {
//		trace("insertBefore: c=", c, ", value=",value);
		for (;;) {
			updateCursor(c);
	//		System.out.println("Updated");
			if (c.invDel || c.invIns) {
//				System.out.println("FAILING D=" + c.invDel + " I=" + c.invIns);
				return false;
			}
			// unroll these fellas
			Node [] nodes = {c.prev(), c.node, c.next()};
			Info [] infos = {nodes[0].info, nodes[1].info, nodes[2].info};
			
			if (checkInfo(nodes, infos)) {
				Node<E> newNode = new Node<>(value, null, c.node.prev, null, DUMMY, ORDINARY);
				Node<E> copy = new Node<E>(c.node.value, c.node.next, newNode, null, DUMMY, ORDINARY);
				newNode.next = copy;
				Info<E> info = new Info<E>(nodes, infos, newNode, copy, false, IN_PROGRESS);
				
				if (help(info)) {
					c.node = copy;
					return true;
				}
			}
		}
	}
	
	
	protected boolean insertBefore1(Cursor <E> c, E value) {
//		trace("insertBefore1: c=", c, ", value=",value);
		for (;;) {
			updateCursor(c);
	//		System.out.println("Updated");
			if (c.invDel || c.invIns) {
//				System.out.println("FAILING D=" + c.invDel + " I=" + c.invIns);
				return false;
			}
			
			Node <E> np = c.prev();
			Node <E> nc = c.node;
			Node <E> nn = c.next();
			
			Info <E> ip = np.info;
			Info <E> ic = nc.info;
			Info <E> in = nn.info;
			
			if (checkInfo1(np, nc, nn, ip, ic, in)) {
				Node<E> newNode = new Node<>(value, null, np, null, DUMMY, ORDINARY);
				Node<E> copy = new Node<E>(nc.value, nn, newNode, null, DUMMY, ORDINARY);
				newNode.next = copy;
				@SuppressWarnings("unchecked")
				Info<E> info = new Info<E>(new Node[] {np, nc, nn}, new Info []{ip, ic, in}, newNode, copy, false, IN_PROGRESS);
				
				if (help(info)) {
					c.node = copy;
					return true;
				}
			}
		}
	}
	
	protected BOOL moveLeft (Cursor<E> c) {
//		trace("moveLeft: c=", c);
		for (;;) {
			updateCursor(c);
	
			if (c.invDel) continue;
//			if (c.invDel) return INVALIDCURSOR;
			if (c.prev() == HEAD) return FALSE;
			if (c.prev().prev.next != c.prev()
			 && c.prev().next == c.node)
			{
				if (c.prev().state == COPIED) {
					c.node = c.prev().copy;
				}
				else {
					Node<E> ppn = c.prev().prev;
					if (ppn == HEAD) return FALSE;
					c.node = ppn;
				}
			}
			else {
				c.node = c.prev();
				break;
			}
		}
		return TRUE;
	}
	
	protected void print() {
		Cursor<E> c = resetCursor();
		
		System.out.print("HEAD -> ");
		do {
			System.out.print(c.node + "(" +
					(c.invDel ? 't' : 'f') +
					(c.invIns ? 't' : 'f') + ") -> ");
		} while (moveRight(c));
		System.out.println("TAIL");
	}
	
	private boolean checkInfo(Node<E> nodes[], Info <E> info []) {
		// we can unroll these loops
		for (int i = 0; i < 3; i++) {
			if (info[i].state == IN_PROGRESS) {
				help(info[i]);
				return false;
			}
		}
		
		for (int i = 0; i < 3; i++) {
			if (nodes[i].state != ORDINARY) {
				return false;
			}
		}
		if (nodes[1].info != info[1]) return false;
		if (nodes[2].info != info[2]) return false;
		return true;
	}
	
	private boolean checkInfo1(Node <E> np, Node <E> nc, Node <E> nn, Info<E> ip, Info<E> ic, Info<E> in) {
		
		if (ip.state == IN_PROGRESS) {help(ip); return false;}
		if (ic.state == IN_PROGRESS) {help(ic); return false;}
		if (in.state == IN_PROGRESS) {help(in); return false;}
		
		if (np.state != ORDINARY) {return false;}
		if (nc.state != ORDINARY) {return false;}
		if (nn.state != ORDINARY) {return false;}
		
		if (nc.info != ic) return false;
		if (nn.info != in) return false;
		
		return true;
	}
	
	private boolean help (Info<E> info) {
//		trace("help: info=", info);
		boolean doPtrCas = true;
		int i = 0;
		
		while (i < 3 && doPtrCas) {
			info.nodes[i].casInfo(info.oldInfo[i], info);
			doPtrCas = (info.nodes[i].info == info);
			i++;
		}
		
		if (doPtrCas) {
			if (info.remove) {
				info.nodes[1].state = MARKED;
			}
			else {
				info.nodes[1].copy = info.newPrev;
				info.nodes[1].state = COPIED;
			}
			info.nodes[0].casNext(info.nodes[1], info.newNext);
			info.nodes[2].casPrev(info.nodes[1], info.newPrev);
			info.state = COMITTED;
		}
		else if (info.state == IN_PROGRESS) {
			info.state = ABORTED;
		}
		return info.state == COMITTED;
	}
	
	@Override
	public boolean addAll(Collection<? extends E> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		Cursor<E> c = resetCursor();
		return !moveRight(c);
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(E e) {
		addLast(e); // XXX ??
		return true;
	}

	@Override
	public void addFirst(E e) {
		while (offerFirst(e) == false) {}
	}

	@Override
	public void addLast(E e) {
		while (offerLast(e) == false) {}
	}

	@Override
	public boolean contains(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<E> descendingIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E element() {
		return first();
	}

	@Override
	public E getFirst() {
		return first();
	}

	@Override
	public E getLast() {
		//return last();
		return null;
	}

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean offer(E e) {
		return offerLast(e);
	}

	@Override
	public boolean offerFirst(E e) {
		Cursor<E> c = resetCursor();
		return insertBefore1(c, e);
	}

	@Override
	public boolean offerLast(E e) {
		Cursor<E> c = setCursor(TAIL.prev);
		boolean rv = insertBefore(c, e);
//		c.invDel = false;
//		c.invIns = false;
		return rv;
	}

	@Override
	public E peek() {
		return first();
	}

	@Override
	public E peekFirst() {
		//return first();
		return null;
	}

	@Override
	public E peekLast() {
		//return last();
		return null;
	}

	protected E poll(Cursor<E> c) {
		return null;
	}
	@Override
	public E poll() {
		return pollFirst();
	}

	@Override
	public E pollFirst() {
		Cursor<E> c = resetCursor();
		return delete1(c);
	}
	
	public E pollLast() {
		return pollLastA();
	}
	public E pollLastA() {
		E rv = null;
		Cursor<E> c = setCursor(TAIL.prev);
		for (;;) {
			BOOL b = moveLeft(c);
//			trace("pollLastA: moveLeft=" + b);
			if (b == TRUE) {
				 rv = delete1(c);
				 /* If delete returns null then the cursor is at EOL. This means
				  * that either the list is empty, or updateCursor has moved the
				  * cursor due to a deleted adjacent node. To account for the
				  * latter condition we continue the loop until it is no longer
				  * possible to move left. Then when c == EOL && moveLeft == false
				  * we exit the loop.
				  */
				 if (rv != null)
					 break;
			}
			else
				break;
		}
		return rv;
	}
	// doesn't share well
	public E pollLast3 (){
		E rv = null;
		for (;;) {
			rv = last();
			if (rv == null)
				break; // return null
			BOOL b = delete2(cursor.get());
			if (b == TRUE)
				break;
//			else
//				trace ("pollLast b=", b);
		}
		return rv;
	}

//	@Override
	public E pollLast1() {
		E rv = null;
		OUTER:
		for (;;) {
			Cursor<E> c = setCursor(TAIL.prev); // EOL
			updateCursor(c);
			for (;;) {
				BOOL b = moveLeft(c);
				if (b == TRUE) {
					break;
				}
				else if (b == FALSE) {
//					trace("Cant move left: ", c);
					return null;
				}
				else if (b == INVALIDCURSOR) {
					continue OUTER; // maybe outer ?
				}
			}
			rv = delete1(c);
			if (rv != INVALID)
				break;
//			trace("got invalid cursor");
			continue;
		}
		return rv;
	}
	
	public E pollLastX() {
		E rv = null;
		Cursor<E> c;
		OUTER:
		for (;;) {
			c = setCursor(TAIL.prev); // EOL
			for (;;) {
				BOOL b = moveLeft(c);
				if (b == TRUE) {
					rv = get(c);
					if (rv == null)
						continue OUTER;
					else
						break;
				}
				else if (b == FALSE) {
//					trace("Cant move left: ", c);
					return null;
				}
				else if (b == INVALIDCURSOR) {
					continue;
				}
			}
			break;
		}
		for (;;) {
			BOOL b = delete2(c);
			if (b == INVALIDCURSOR) {
//				trace("got invalid cursor");
				//return null;
				continue;
			}
			else if (b == FALSE)
				return null;
			else
				break;
//			trace("got invalid cursor");
		}
		return rv;
	}

	@Override
	public E pop() {
		return removeFirst();
	}

	@Override
	public void push(E e) {
		addFirst(e);
	}

	@Override
	public E remove() {
		return removeFirst();
	}

	@Override
	public boolean remove(Object o) {
		return false;
	}

	@Override
	public E removeFirst() {
		E e = pollFirst();
		if (e != null) {
			return e;
		}
		throw new NoSuchElementException();
	}

	@Override
	public boolean removeFirstOccurrence(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E removeLast() {
		E e = pollLast();
		if (e != null) {
			return e;
		}
		throw new NoSuchElementException();
	}

	@Override
	public boolean removeLastOccurrence(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		int n = 0;
		Cursor <E> c = resetCursor();
		for ( ;moveRight(c); n++) {}
		return n;
	}
	
	/**
	 * 
	 */
	static final class Node <E> {
		static sun.misc.Unsafe unsafe;
		/**
		 * The next node in the list
		 */
		Node <E> next;
		
		/**
		 * The previous node in the list;
		 */
		Node <E> prev;
		
		/**
		 * The copy of the node if it is being replaced
		 */
		Node <E> copy;
		
		/**
		 * The payload of this node
		 */
		final E value;
		
		/**
		 * The current state of the Node
		 */
		State state = ORDINARY;
		
		Info<E> info;
		
		/**
		 * Constructs a new list Node
		 * 
		 * @param value The payload of the Node.
		 * @param next The next Node in the list.
		 * @param prev The previous Node in the list
		 * @param copy The copy if the list if being replaced.
		 * @param info The Node info object.
		 * @param state The current Node state.
		 */
		Node(E value, Node<E> next, Node<E> prev, Node<E> copy, Info<E> info, State state) {
			this.value = value;
			this.next = next;
			this.prev = prev;
			this.copy = copy;
			this.info = info;
			this.state = state;
		}
		
		public String toString() {
			StringBuffer buffer = new StringBuffer();
			buffer.append(value == null ? "null" : value.toString());
			buffer.append('(');
			buffer.append(state.name().charAt(0));
			buffer.append(')');
			buffer.append(" [P=");
			buffer.append(prev == null ? "(null)" : prev.value);
			buffer.append(", N=");
			buffer.append(next == null ? "(null)" : next.value);
			buffer.append("]");
			return buffer.toString();
		}

		public boolean casInfo (Info<E> cmp, Info<E> swap) {
			return unsafe.compareAndSwapObject(this, INFO_OFFSET, cmp, swap);
		}
		
		public boolean casNext (Node<E> cmp, Node<E> swap) {
			return unsafe.compareAndSwapObject(this, NEXT_OFFSET, cmp, swap);
		}
		
		public boolean casPrev (Node<E> cmp, Node<E> swap) {
			return unsafe.compareAndSwapObject(this, PREV_OFFSET, cmp, swap);
		}
		
		private static long INFO_OFFSET;
		private static long NEXT_OFFSET;
		private static long PREV_OFFSET;
		
		static {
			Class<?> k = Node.class;
			
			try {
				Constructor <Unsafe> c = Unsafe.class.getDeclaredConstructor();
				c.setAccessible(true);
				unsafe = c.newInstance();
				INFO_OFFSET = unsafe.objectFieldOffset(k.getDeclaredField("info"));
				NEXT_OFFSET = unsafe.objectFieldOffset(k.getDeclaredField("next"));
				PREV_OFFSET = unsafe.objectFieldOffset(k.getDeclaredField("prev"));
				
			} catch (NoSuchMethodException | SecurityException | InstantiationException
					| IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private static final class Info <E> {
		
		@SuppressWarnings("unchecked")
		Node<E> [] nodes = new Node[3];
		
		@SuppressWarnings("unchecked")
		Info<E> [] oldInfo = new Info[3];
		
		Node<E> newNext;
		
		Node<E> newPrev;
		
		boolean remove = false;
		
		State state = IN_PROGRESS;

		
		/**
		 * 
		 * @param nodes
		 * @param info
		 * @param next set nodes[0].next to this
		 * @param prev set nodes[2].prev to this
		 * @param remove true if nodes[1] is being deleted
		 * @param state
		 */
		Info (Node<E> [] nodes, Info<E> [] info, Node <E> next, Node<E>  prev, boolean remove, State state) {
			this.nodes = nodes;
			this.oldInfo = info;
			this.newNext = next;
			this.newPrev = prev;
			this.remove = remove;
			this.state = state;
		}
		
		public String toString() {
			StringBuffer buffer = new StringBuffer();
			buffer.append("[newNext=");
			buffer.append(newNext.value);
			buffer.append(", newPrev=");
			buffer.append(newPrev.value);
			buffer.append(", remove=");
			buffer.append(remove);
			buffer.append(", state=");
			buffer.append(state);
			buffer.append("]");
			return buffer.toString();
		}
		

	}
	
	/**
	 * Per thread cursor object
	 * @author tim
	 *
	 * @param <E>
	 */
	private static final class Cursor<E> {
		
		Node<E> node;
		
		String name;
		
		boolean invDel;
		
		boolean invIns;
		
		Cursor(Node<E> node, boolean invDel, boolean invIns) {
			this.node = node;
			this.invDel = invDel;
			this.invIns = invIns;
		}
		
		public Node<E> prev() {
			return node.prev;
		}
		
		public Node<E> next() {
			return node.next;
		}
		
		public String toString() {
			return node.toString();
		}
	}

	static enum State {
			COPIED,
			MARKED,
			ORDINARY,
			IN_PROGRESS,
			COMITTED,
			ABORTED;
	}
}

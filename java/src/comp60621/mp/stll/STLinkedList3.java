package comp60621.mp.stll;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicMarkableReference;


public class STLinkedList3<E> implements Deque<E> {

	private final boolean TRACE;
	
	private final Node HEAD ;
	private final Node TAIL;
	
	public STLinkedList3() {
		this(false);
	}
	
	@SuppressWarnings("unchecked")
	public STLinkedList3(boolean trace) {
		HEAD = new Node((E)"HEAD", null, null);
		TAIL = new Node((E)"TAIL", HEAD, null);
		HEAD.setNext(TAIL, false);
		TRACE = trace;
	}
	
	Node correctPrev(Node prev, Node node) {
//		trace("correctPrev: prev=" + prev, ", node=", node);
		Node rv = null;
		Node lastlink = null;
		boolean [] del = {false};
		
		for (;;) {
			Node link1 = node.prev(del);
			if (del[0]) { break; }
			Node prev2 = prev.next();
			if (prev.nextDelete()) {
				if (lastlink != null) {
					markPrev(prev);
					lastlink.casNext(prev, false, prev2, false);
					prev = lastlink;
					lastlink = null;
					continue;
				}
				prev = prev.prev();
				continue;
			}
			if (prev2 != node) {
				lastlink = prev;
				prev = prev2;
				continue;
			}
			if (node.casPrev(link1, false, prev, false)) {
				if (prev.prevDelete()) continue;
				break;
			}
		}
		return prev;
	}
	
	void pushEnd(Node node, Node next) {
//		trace("pushEnd: node=", node, ", next=", next);
		for (;;) {
			boolean [] del = {false};
			Node link1 = next.prev(del);
			if (del[0] || node.next() != next || node.nextDelete())
				break;
			if (next.casPrev(link1, del[0], node, false)) {
				if (node.prevDelete()) {
					node = correctPrev(node, next);
				}
				break;
			}
		}
	}
	
	E popLeft() {
		E rv = null;
		Node prev = HEAD;
		boolean [] del = {false};
		for (;;) {
			Node node = prev.next();
			
			if (node == TAIL) { 
				break; // i.e. return null
			}
			
			Node next = node.next(del);
			if (del[0]) {
				markPrev(node);
				prev.casNext(node, false, next, false);
				continue;
			}
			if (node.casNext(next, false, next, true)) {
				prev = correctPrev(prev, next);
				rv = node.value;
				break;
			}
		}
		return rv;
	}
	
	E popRight() {
		E rv = null;
		Node next = TAIL;
		Node node = next.prev();
		boolean [] del = {false};
		for (;;) {
			node = next.prev();
//			if (node == null) {System.out.println("popRight NP encountered");continue;}
			Node test = node.next(del);
			if (test != next || del[0]) {
				node = correctPrev(node, next);
				continue;
			}
			if (node == HEAD) {
				return null;
			}
			if (node.casNext(next, false, next, true)) {
				Node prev = node.prev();
				prev = correctPrev(prev, next);
				rv = node.value;
				break;
			}
		}
		return rv;
	}
	
	void pushLeft(E e) {
		Node node = new Node(e);
		Node prev = HEAD;
		Node next;
		
		for (;;) {
			next = prev.next();
			node.setPrev(prev, false);
			node.setNext(next, false);
			
			if (prev.casNext(next, false, node, false))
				break;
		}
		pushEnd(node, next);
	}
	
	void pushRight(E e) {
		Node node = new Node(e);
		Node next = TAIL;
		Node prev = next.prev();
		
		for (;;) {
			prev = next.prev();
			node.setPrev(prev, false);
			node.setNext(next, false);
			
			if (prev.casNext(next, false, node, false))
				break;
			prev = correctPrev(prev, next);
		}
		pushEnd(node, next);
	}
	// need to look at this
	void markPrev(Node node) {
//		trace("markPrev: " + node);
		for (;;) {
			Node prev = node.prev();
			if (node.prevDelete() || node.casPrev(prev, false, prev, true)) {
				break;
			}
		}
	}
	
	void trace(String message, Object ... args) {
		if (TRACE) {
			System.out.print(message);
			if (args != null && args.length != 0) {
				for (Object o : args) {
					System.out.print(o == null ? "null" : o);
				}
			}
			System.out.print('\n');
		}
	}
	
	private class Node {
		private E value;
		
		private AtomicMarkableReference<Node> nextRef;
		
		private AtomicMarkableReference<Node> prevRef;
		
		private Node(E value) {
			this(value, null, false, null, false);
		}
		
		private Node(E value, Node prev, Node next) {
			this(value, prev, false, next, false);
		}
		
		private Node(E value, Node prev, boolean prevDel, Node next, boolean nextDel) {
			this.value = value;
			nextRef = new AtomicMarkableReference<>(next, nextDel);
			prevRef = new AtomicMarkableReference<>(prev, prevDel);
		}
		
		private E value() {
			return value;
		}
		
		private Node dnext() {
			return nextDelete() ? null : next();
		}
		
		private Node dprev() {
			return prevDelete() ? null : prev();
		}
		
		private Node next() {
			return nextRef.getReference();
		}
		
		private Node prev() {
			return prevRef.getReference();
		}
		
		private Node next(boolean [] del) {
			return nextRef.get(del);
		}
		
		private Node prev(boolean [] del) {
			return prevRef.get(del);
		}
		
		private void setNext(Node next, boolean delete) {
			//nextRef = new AtomicMarkableReference<>(next, delete);
			nextRef.set(next, delete);
		}
		
		private void setPrev(Node prev, boolean delete) {
			//prevRef = new AtomicMarkableReference<>(next, delete);
			prevRef.set(prev, delete);
		}
		
		private boolean nextDelete() {
			return nextRef.isMarked();
		}
		
		private boolean prevDelete() {
			return prevRef.isMarked();
		}
		
		private boolean casNext(Node expected, boolean expectedDelete, Node node, boolean delete) {
			return nextRef.compareAndSet(expected, node, expectedDelete, delete);
		}
		
		private boolean casPrev(Node expected, boolean expectedDelete, Node node, boolean delete) {
			return prevRef.compareAndSet(expected, node, expectedDelete, delete);
		}
		
		public String toString() {
			return value
				+ " [P=" + (prev() == null ? "null" : prev().value) + (prevDelete() ? "(d)" : "")
				+ " N=" + (next() == null ? "null" : next().value) + (nextDelete() ? "(d)]" : "]");
		}
		
	}
	@Override
	public boolean addAll(Collection<? extends E> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		return HEAD.next() == TAIL;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(E e) {
		addLast(e);
		return true;
	}

	@Override
	public void addFirst(E e) {
		pushLeft(e);
		
	}

	@Override
	public void addLast(E e) {
		pushRight(e);
		
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<E> descendingIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E element() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E getFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E getLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean offer(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean offerFirst(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean offerLast(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E peek() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E peekFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E peekLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E poll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E pollFirst() {
		return popLeft();
	}

	@Override
	public E pollLast() {
		return popRight();
	}

	@Override
	public E pop() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void push(E e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public E remove() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E removeFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeFirstOccurrence(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E removeLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeLastOccurrence(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

}

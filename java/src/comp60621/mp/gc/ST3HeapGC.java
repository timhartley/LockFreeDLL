package comp60621.mp.gc;

import java.util.concurrent.ConcurrentLinkedDeque;

import comp60621.mp.stll.STLinkedList3;

public class ST3HeapGC {

	private static final int NTHREADS = 8;
	private static final int NELEMENTS = 1000000;
	private STLinkedList3<Integer> list = new STLinkedList3<>();		
	private Thread [] THREADS = new Thread[NTHREADS];
	
	public static void main(String[] args) {
		ST3HeapGC x = new ST3HeapGC();
		x.run();
		System.gc();
		System.exit(0);

	}
	
	public void run () {
		
		for (int i = 0; i < NELEMENTS; i++) {
			list.addFirst(i);
		}
		
		for (int i = 0; i < NTHREADS; i++) {
			THREADS[i] = new Thread(new Runnable() {

				@Override
				public void run() {
					poll();
				}
			});
		}
		
		for (int i = 0; i < NTHREADS; i++) {
			THREADS[i].start();
		}
		
		for (int i = 0; i < NTHREADS; i++) {
			try {
				THREADS[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

	private void poll() {
		Integer i;
		for (;;) {
			i = list.pollFirst();
			if (i == null) {
				break;
			}
		}
	}
}

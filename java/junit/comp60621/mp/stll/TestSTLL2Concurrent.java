package comp60621.mp.stll;

import static org.junit.Assert.*;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestSTLL2Concurrent {

	private STLinkedList2 <Integer> list;
	private ConcurrentLinkedQueue <Integer> collector;
	
	@Before
	public void setUp() throws Exception {
		list = new STLinkedList2<>(false);
		collector = new ConcurrentLinkedQueue<Integer>();
	}
	
	private void removeFirst () {
		Integer i;
		for (;;) {
			i = list.pollFirst();
			if (i == null) break;
			//System.out.println("ID=" + Thread.currentThread().getId() + ", i=" + i);
			collector.add(i);
		}
	}
	
	private void removeLast () {
		Integer i;
		for (;;) {
			i = list.pollLast();
			if (i == null) break;
			//System.out.println("ID=" + Thread.currentThread().getId() + ", i=" + i);
			collector.add(i);
		}
	}
	
	private void startThreads(Thread [] threads) {
		for (Thread t : threads) {
			t.start();
		}
	}

	private void joinThreads(Thread [] threads) {
		for (Thread t : threads) {
			try {
				t.join();
				System.out.println("Joined thread: " + t.getId());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private static final int ELEMENTS = 10000;	@Test
	public void test_concurrent_remove_first() {
		RemoveFirst [] threads = new RemoveFirst[8];
		int checksum = 0, checksum2 = 0;
		for (int i = 0; i < ELEMENTS; i++) {
			list.addFirst(i);
			checksum += i;
		}
		
		for (int i = 0; i < 8; i++) {			
			threads[i] = new RemoveFirst(list);	
			System.out.println("Created Thread=" + threads[i].getId());
		}
		
		startThreads(threads);
		for (RemoveFirst t : threads) {
			try {
				t.join();
				System.out.println("Joined thread: " + t.getId());
				checksum2 += t.checksum();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		assertEquals(checksum, checksum2);
	}
	
	@Test
	public void test_concurrent_remove_last() {
		Thread [] threads = new Thread[8];
		int checksum = 0;
		for (int i = 0; i < ELEMENTS; i++) {
			list.addFirst(i);
			checksum += i;
		}
		
		for (int i = 0; i < 8; i++) {			
			threads[i] = new Thread(() -> removeLast());	
			System.out.println("Created Thread=" + threads[i].getId());
		}
		
		startThreads(threads);
		joinThreads(threads);
		
		assertEquals(ELEMENTS, collector.size());
		int checksum2 = 0;
		for (Integer i : collector) {
			checksum2 += i;
		}
		assertEquals(checksum, checksum2);
//		System.out.println("Help insert called: " + list.traceHelpInsert.get());
//		System.out.println("CAS failed: " + list.traceCASFail.get());
	}
	
	private void addFirstFromCollector() {
		for (;;) {
			Integer i = collector.poll();
			if (i == null) {
				break;
			}
			list.addFirst(i);
		}
	}
	
	
	private void addLastFromCollector() {
		for (;;) {
			Integer i = collector.poll();
			if (i == null) {
				break;
			}
			list.addLast(i);
		}
	}
	
//	@Test
	public void test_concurrent_add_first() {
		Thread [] threads = new Thread[8];
		int checksum = 0;
		
		for (int i = 0; i < ELEMENTS; i++) {
			collector.add(i);
			checksum += i;
		}
		
		
		for (int i = 0; i < 8; i++) {			
			threads[i] = new Thread(() -> addFirstFromCollector());	
			System.out.println("Created Thread=" + threads[i].getId());
		}
		startThreads(threads);
		joinThreads(threads);
		
		int checksum2 = 0;
		for (;;) {
			Integer i = list.pollFirst();
			if (i == null) break;
			checksum2 += i;
		}
		assertEquals(checksum, checksum2);
	}
	
	@Test
	public void test_concurrent_add_last() {
		Thread [] threads = new Thread[8];
		int checksum = 0;
		
		for (int i = 0; i < ELEMENTS; i++) {
			collector.add(i);
			checksum += i;
		}
		
		
		for (int i = 0; i < 8; i++) {			
			threads[i] = new Thread(() -> addLastFromCollector());	
			System.out.println("Created Thread=" + threads[i].getId());
		}
		startThreads(threads);
		joinThreads(threads);
		
//		assertEquals(ELEMENTS, list.size());
		int checksum2 = 0;
		for (;;) {
			Integer i = list.pollFirst();
			if (i == null) break;
			checksum2 += i;
		}
		assertEquals(checksum, checksum2);
//		System.out.println("Help insert called: " + list.traceHelpInsert.get());
//		System.out.println("CAS failed: " + list.traceCASFail.get());
	}
	
	@After
	public void tearDown() throws Exception {
	}

	private class RemoveFirst extends Thread {
		
		private int checksum = 0;
		private final Deque<Integer> deque;
		
		RemoveFirst(Deque<Integer> dq) {
			deque = dq;
		}
		
		int checksum() {
			return checksum;
		}
	

		@Override
		public void run() {
			long start = System.nanoTime();
			for (;;) {
				Integer i = deque.pollFirst();
				if (i == null) break;
				checksum += i;
			}			
		}
	}
}

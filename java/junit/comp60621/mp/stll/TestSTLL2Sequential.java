package comp60621.mp.stll;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestSTLL2Sequential {

	private STLinkedList2 <Integer>list;
	@Before
	public void setUp() throws Exception {
		list = new STLinkedList2<>();
	}

	@Test
	public void test_is_empty() {
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void test_push_left() {
		list.pushLeft(1);
		assertFalse(list.isEmpty());
	}
	
	@Test
	public void test_push_left_pop_left() {
		Integer one = new Integer(1);
		list.pushLeft(one);
		assertEquals(one, list.popLeft());
		
	}
	
	@Test
	public void test_push_left_pop_left_1000() {
		for (int i = 0; i < 1000; i++) {
			list.pushLeft(i);
		}
		
		for (int i = 999; i > 0; i--) {
			assertEquals(new Integer(i), list.popLeft());
		}
	}
	
	@Test
	public void  test_push_right() {
		list.pushRight(1);
		assertFalse(list.isEmpty());
	}
	
	@Test
	public void test_push_right_pop_right() {
		list.pushRight(1);
		assertEquals(new Integer(1), list.popRight());
	}
	
	@Test
	public void test_push_right_pop_right_1000() {
		for (int i = 0; i < 1000; i++) {
			list.pushRight(i);
		}
		
		for (int i = 999; i > 0; i--) {
			assertEquals(new Integer(i), list.popRight());
		}
		
		assert(list.isEmpty());
	}
	@After
	public void tearDown() throws Exception {
	}

//	@Test
	public void test() {
		fail("Not yet implemented");
	}

}

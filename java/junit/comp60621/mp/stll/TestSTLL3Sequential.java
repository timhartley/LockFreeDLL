package comp60621.mp.stll;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestSTLL3Sequential {
	
	private STLinkedList3<Integer> list;

	@Before
	public void setUp() throws Exception {
		list = new STLinkedList3<>(true);
	}

	@After
	public void tearDown() throws Exception {
	}

//	@Test
	public void test_push_left() {
		list.pushLeft(1);
		assertFalse(list.isEmpty());
	}
	
	
//	@Test
	public void test_push_left_pop_left() {
		list.pushLeft(1);
		assertEquals(new Integer(1), list.popLeft());
		assertTrue(list.isEmpty());
	}
	
//	@Test
	public void test_push_left_pop_left_1000() {
		for (int i = 0; i < 1000; i++) {
			list.pushLeft(i);
		}
		
		for (int i = 999; i >= 0; i--) {
			assertEquals(new Integer(i), list.popLeft());
		}
		
		assertTrue(list.isEmpty());
	}
//	@Test
	public void test_empty() {
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void test_push_left_10_then_pop() {
		for (int i = 0; i < 10; i++) {
			list.pushLeft(i);
		}
		
		for (;;) {
			Integer i = list.popLeft();
			if (i == null)
				break;
		}
		assertTrue(list.isEmpty());
	}
	

}

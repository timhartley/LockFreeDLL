package comp60621.mp.stll;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestSTLLSequential {

	private STLinkedList<Integer> list;
	
	@Before
	public void setUp() throws Exception {
		list = new STLinkedList<>();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_push_left() {
		list.pushLeft(1);
//		list.print("test_push_left: ", System.out);
		assertFalse(list.isEmpty());
//		for (int i = 0; i < 1000; i++) {
//			list.pushLeft(new Integer(i));
//		}
	}
	
	@Test
	public void test_push_left_pop_left() {
		list.pushLeft(1);
//		list.print("test_push_left_pop_left (after push): ", System.out);
		assertEquals(new Integer(1), list.popLeft());
//		list.print("test_push_left_pop_left: ", System.out);
		assertTrue(list.isEmpty());
	}
	
	
	@Test
	public void test_push_left_pop_left_2() {
		list.pushLeft(1);
		list.pushLeft(2);
		assertEquals(new Integer(2), list.popLeft());
		assertEquals(new Integer(1), list.popLeft());
//		list.print("test_push_left_pop_left_2: ", System.out);
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void test_push_left_pop_left_10() {
		for (int i = 0; i < 10; i++) {
			list.pushLeft(i);
		}
		for (int i = 9; i >= 0; i--) {
			assertEquals(new Integer(i), list.popLeft());
		}
//		list.print(System.out);
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void test_push_left_pop_left_1000() {
		for (int i = 0; i < 1000; i++) {
			list.pushLeft(i);
		}
		for (int i = 999; i >= 0; i--) {
			assertEquals(new Integer(i), list.popLeft());
		}
//		list.print(System.out);
		assertTrue(list.isEmpty());
	}
//	@Test
	public void test() {
		fail("Not yet implemented");
	}

}

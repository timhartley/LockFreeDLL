package comp60621.mp.ndll;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;

import comp60621.mp.ndll.NDLinkedList.Node;

public class TestNDLinkedList {

	NDLinkedList <Integer> list;
	
	@Before
	public void setUp() {
		list = new NDLinkedList<>();
	}
	
	@Test
	public void testNew() {
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void test_offer_first_to_empty_list() {
		Integer i = 1;
		assertTrue(list.offerFirst(i));
		list.print();
		assertEquals(i, list.first());
	}
	
	@Test
	public void test_offer_first_then_pop_left() {
		Integer i = 1;
		assertTrue(list.offerFirst(i));
		Integer j = list.popLeft();
		//list.print();
		assertEquals(i, j);
		assertTrue(list.isEmpty());
	}
	
	
	@Test
	public void test_offer_first_then_last() {
		assertTrue(list.offerFirst(1));
		assertEquals(new Integer(1), list.last());
	}
	
	@Test
	public void test_offer_last_then_last() {
		assertTrue(list.offerLast(1));
		assertEquals(new Integer(1), list.last());
	}
	
	@Test
	public void test_node_cas_next() {
		Node<Integer> next = new Node<>(null, null, null, null, null, null);
		Node<Integer> node = new Node<>(null, next, null, null, null, null);
		assertTrue(node.casNext(next, node));
		assertEquals(node.next, node);
	}
	
	@Test
	public void test_node_cas_prev() {
		Node<Integer> prev = new Node<>(null, null, null, null, null, null);
		Node<Integer> node = new Node<>(null, null, prev, null, null, null);
		assertTrue(node.casPrev(prev, node));
		assertEquals(node.prev, node);
	}
	
	@Test
	public void test_add_last_ten_then_remove_last() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Adding: " + i);
			list.addLast(i);
			list.print();
		}
		
		for (int i = 9; i >= 0; i--) {
			assertEquals(new Integer(i), list.removeLast());
		}
		assertTrue(list.isEmpty());
	}

	@Test
	public void test_add_then_poll_then_poll_returns_null () {
		assertTrue(list.add(1));
		assertEquals(new Integer(1), list.poll());
		assertEquals(null, list.poll());
		assertTrue(list.isEmpty());
	}
	@Test
	public void test_poll_first_on_empty_list_returns_null () {
		assertEquals(null, list.pollFirst());
	}
	
	@Test(expected=NoSuchElementException.class)
	public void test_remove_first_on_empty_list_throws_exception () {
		list.removeFirst();
	}
	
	@Test
	public void test_add_ten_first_then_remove_first() {
		for (int i = 0; i < 10; i++) {
			list.addFirst(i);
		}
		
		for (int i = 9; i >= 0; i--) {
			assertEquals(new Integer(i), list.removeFirst());
		}
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void test_add_ten_first_then_remove_last() {
		for (int i = 0; i < 10; i++) {
			list.addFirst(i);
		}
		
		for (int i = 0; i < 10; i++) {
			assertEquals(new Integer(i), list.removeLast());
		}
		list.print();
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void test() {
		fail("Not yet implemented");
	}

}

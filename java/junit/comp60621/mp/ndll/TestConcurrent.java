package comp60621.mp.ndll;

import static org.junit.Assert.*;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestConcurrent {

	private NDLinkedList<Integer> list;
	
	private ConcurrentLinkedQueue <Integer> collector;
	@Before
	public void setUp() throws Exception {
		list = new NDLinkedList<>();
		collector = new ConcurrentLinkedQueue<Integer>();
	}
	
	private void removeFirst () {
		Integer i;
		for (;;) {
			i = list.pollFirst();
			if (i == null) break;
			//System.out.println("ID=" + Thread.currentThread().getId() + ", i=" + i);
			collector.add(i);
		}
	}
	
	private void removeLast () {
		Integer i;
		for (;;) {
			i = list.pollLast();
			if (i == null) break;
			//System.out.println("ID=" + Thread.currentThread().getId() + ", i=" + i);
			collector.add(i);
		}
	}
	
	private void startThreads(Thread [] threads) {
		for (Thread t : threads) {
			t.start();
		}
	}

	private void joinThreads(Thread [] threads) {
		for (Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private static final int ELEMENTS = 10000;
//	@Test
	public void test_concurrent_remove_first() {
		Thread [] threads = new Thread[8];
		int checksum = 0;
		for (int i = 0; i < ELEMENTS; i++) {
			list.addFirst(i);
			checksum += i;
		}
		
		for (int i = 0; i < 8; i++) {			
			threads[i] = new Thread(() -> removeFirst());	
			System.out.println("Created Thread=" + threads[i].getId());
		}
		
		startThreads(threads);
		joinThreads(threads);
		
		assertEquals(ELEMENTS, collector.size());
		int checksum2 = 0;
		for (Integer i : collector) {
			checksum2 += i;
		}
		assertEquals(checksum, checksum2);
	}
	
	@Test
	public void test_concurrent_remove_last() {
		Thread [] threads = new Thread[8];
		int checksum = 0;
		for (int i = 0; i < ELEMENTS; i++) {
			list.addFirst(i);
			checksum += i;
		}
		
		for (int i = 0; i < 8; i++) {			
			threads[i] = new Thread(() -> removeLast());	
			System.out.println("Created Thread=" + threads[i].getId());
		}
		
		startThreads(threads);
		joinThreads(threads);
		
		assertEquals(ELEMENTS, collector.size());
		int checksum2 = 0;
		for (Integer i : collector) {
			checksum2 += i;
		}
		assertEquals(checksum, checksum2);
	}
	
	private void addFirstFromCollector() {
		for (;;) {
			Integer i = collector.poll();
			if (i == null) {
				break;
			}
			list.addFirst(i);
		}
	}
	
	
	private void addLastFromCollector() {
		for (;;) {
			Integer i = collector.poll();
			if (i == null) {
				break;
			}
			list.addLast(i);
		}
	}
	
//	@Test
	public void test_concurrent_add_first() {
		Thread [] threads = new Thread[8];
		int checksum = 0;
		
		for (int i = 0; i < ELEMENTS; i++) {
			collector.add(i);
			checksum += i;
		}
		
		
		for (int i = 0; i < 8; i++) {			
			threads[i] = new Thread(() -> addFirstFromCollector());	
			System.out.println("Created Thread=" + threads[i].getId());
		}
		startThreads(threads);
		joinThreads(threads);
		
		int checksum2 = 0;
		for (;;) {
			Integer i = list.pollFirst();
			if (i == null) break;
			checksum2 += i;
		}
		assertEquals(checksum, checksum2);
	}
	
	// broken
//	@Test
	public void test_delete_returns_invalid_cursor() throws InterruptedException {
		list.add(12);
		assertFalse(list.isEmpty());
		Integer x = list.first();
		System.out.println("X=" + x);
		assertEquals(new Integer(12), x);
		
		Thread t = new Thread(() -> list.pollFirst());
		t.run();
		t.join();
		System.out.println("XXX=" + list.first());
	}
//	@Test
	public void test_concurrent_add_last() {
		Thread [] threads = new Thread[8];
		int checksum = 0;
		
		for (int i = 0; i < ELEMENTS; i++) {
			collector.add(i);
			checksum += i;
		}
		
		
		for (int i = 0; i < 8; i++) {			
			threads[i] = new Thread(() -> addLastFromCollector());	
			System.out.println("Created Thread=" + threads[i].getId());
		}
		startThreads(threads);
		joinThreads(threads);
		
		assertEquals(ELEMENTS, list.size());
		int checksum2 = 0;
		for (;;) {
			Integer i = list.pollFirst();
			if (i == null) break;
			checksum2 += i;
		}
		assertEquals(checksum, checksum2);
	}
	
	@After
	public void tearDown() throws Exception {
	}



}
